# **oneself_music**

## 介绍
oneself_music是采用SpringBoot,Maven,MySql,vue3,element-plus等框架开发的，

前后端分离，客户端，后台管理端组成。后台可以增删改查等等操作，后台增删改查客户端也会改变。

### 项目框架

#### 后端架构

##### 后端开发工具

- IDE: IDEA 2021.3.3

##### 后端技术选型

- SpringBoot
- Maven
- Webscoket
- swagger

##### 前端开发工具

- IDE: webStorm 2021.3.3  当然可以用别的

##### 前端技术选型

- vue3.2.13
- vuex4.0.0
- vue-router4.0.3
- element-plus2.0.4
- axios0.26.0

#### 后端运行

| ![](img/00.png) | sql文件都准备好了，resources里面的application.properties文件里更改数据库配置，启动mysql新建数据库，数据库名字根据自己起都可以，我这里是tp_music，然后找到resources里面的application.properties文件，修改数据库配置，找到主类YinMusicApplication运行就可以了 |
| ---------------------------------------------- | ------------------------------------------------------------ |
|                                                |                                                              |

#### 前端运行

~~~apl
安装项目依赖
```
npm install
```
运行项目
```
npm run serve
```
项目打包
```
npm run build
```

~~~

#### 项目截图

客户端

| ![](img/1.jpg) | ![](img/2.jpg) | ![](img/3.jpg)  |
| ---------------------------------- | ---------------------------------- | ----------------------------------- |
| ![](img/4.jpg) | ![](img/5.jpg) | ![](img/6.jpg)  |
| ![](img/7.jpg) | ![](img/9.jpg) | ![](img/10.jpg) |

后台管理

| ![](img1/1.jpg) | ![](img1/2.jpg) | ![](img1/3.jpg) |
| ----------------------------------- | ----------------------------------- | ----------------------------------- |
| ![](img1/4.jpg) | ![](img1/5.jpg) | ![](img1/6.jpg) |

