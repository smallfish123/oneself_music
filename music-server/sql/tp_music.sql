/*
 Navicat Premium Data Transfer

 Source Server         : MyOder
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : localhost:3306
 Source Schema         : tp_music

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 09/05/2022 12:58:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_UNIQUE`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'adil', 'adil0212');

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL,
  `song_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `song_list_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES (1, 1, 0, 1, NULL, '2022-04-07 17:17:23');
INSERT INTO `collect` VALUES (2, 31, 0, 3, NULL, '2022-04-08 10:45:23');
INSERT INTO `collect` VALUES (3, 7, 0, 136, NULL, '2022-04-14 15:00:41');
INSERT INTO `collect` VALUES (4, 1, 0, 139, NULL, '2022-04-16 13:04:03');
INSERT INTO `collect` VALUES (5, 7, 0, 128, NULL, '2022-04-16 13:05:23');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `song_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `song_list_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `up` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (1, 1, NULL, 1, '21221', '2022-04-07 18:33:35', 1, 1);
INSERT INTO `comment` VALUES (2, 1, NULL, 1, '666', '2022-04-07 19:00:31', 1, 0);
INSERT INTO `comment` VALUES (3, 1, NULL, 2, '', '2022-04-07 19:03:41', 1, 0);
INSERT INTO `comment` VALUES (4, 1, 124, NULL, '挺喜欢这首歌', '2022-04-09 10:12:16', 0, 0);
INSERT INTO `comment` VALUES (5, 1, 136, NULL, '挺好听的，喜欢...', '2022-04-16 13:03:43', 0, 1);
INSERT INTO `comment` VALUES (6, 1, 146, NULL, '挺喜欢这首歌的 全是回忆', '2022-05-07 13:30:00', 0, 0);

-- ----------------------------
-- Table structure for consumer
-- ----------------------------
DROP TABLE IF EXISTS `consumer`;
CREATE TABLE `consumer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` tinyint(4) NULL DEFAULT NULL,
  `phone_num` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` datetime NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_UNIQUE`(`username`) USING BTREE,
  UNIQUE INDEX `phone_num_UNIQUE`(`phone_num`) USING BTREE,
  UNIQUE INDEX `email_UNIQUE`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of consumer
-- ----------------------------
INSERT INTO `consumer` VALUES (1, 'adiljan', 'adil0212', 1, '18599274413', '3307892934@qq.com', '2001-02-12 00:00:00', '管理员账号', '新疆', '/img/avatorImages/16493853414226.jpg', '2022-04-07 18:32:39', '2022-04-07 18:32:39');
INSERT INTO `consumer` VALUES (8, 'mihrigul', '520886', 0, '1023456789', '000@qq.com', '2001-10-10 00:00:00', '一个活泼可爱的女孩子', '新疆', '/img/avatorImages/1651117677795e1fe9925bc315c6034a8cc7b7de1dc1349540923d95c.jpg', '2022-04-16 13:08:55', '2022-04-16 13:08:55');

-- ----------------------------
-- Table structure for list_song
-- ----------------------------
DROP TABLE IF EXISTS `list_song`;
CREATE TABLE `list_song`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `song_id` int(10) UNSIGNED NOT NULL,
  `song_list_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of list_song
-- ----------------------------
INSERT INTO `list_song` VALUES (1, 36, 1);
INSERT INTO `list_song` VALUES (3, 5, 2);
INSERT INTO `list_song` VALUES (4, 7, 2);
INSERT INTO `list_song` VALUES (5, 11, 2);
INSERT INTO `list_song` VALUES (6, 38, 6);
INSERT INTO `list_song` VALUES (7, 39, 6);
INSERT INTO `list_song` VALUES (8, 44, 1);
INSERT INTO `list_song` VALUES (9, 22, 2);
INSERT INTO `list_song` VALUES (10, 22, 12);
INSERT INTO `list_song` VALUES (11, 38, 5);
INSERT INTO `list_song` VALUES (12, 39, 5);
INSERT INTO `list_song` VALUES (13, 38, 5);
INSERT INTO `list_song` VALUES (14, 39, 5);
INSERT INTO `list_song` VALUES (15, 45, 4);
INSERT INTO `list_song` VALUES (16, 45, 12);
INSERT INTO `list_song` VALUES (17, 10, 13);
INSERT INTO `list_song` VALUES (18, 10, 2);
INSERT INTO `list_song` VALUES (19, 28, 3);
INSERT INTO `list_song` VALUES (20, 10, 3);
INSERT INTO `list_song` VALUES (21, 30, 10);
INSERT INTO `list_song` VALUES (22, 31, 10);
INSERT INTO `list_song` VALUES (23, 82, 6);
INSERT INTO `list_song` VALUES (24, 83, 6);
INSERT INTO `list_song` VALUES (25, 84, 6);
INSERT INTO `list_song` VALUES (26, 85, 6);
INSERT INTO `list_song` VALUES (27, 99, 7);
INSERT INTO `list_song` VALUES (28, 100, 8);
INSERT INTO `list_song` VALUES (29, 78, 9);
INSERT INTO `list_song` VALUES (30, 79, 9);
INSERT INTO `list_song` VALUES (31, 80, 9);
INSERT INTO `list_song` VALUES (32, 86, 7);
INSERT INTO `list_song` VALUES (33, 87, 7);
INSERT INTO `list_song` VALUES (34, 88, 8);
INSERT INTO `list_song` VALUES (35, 100, 7);
INSERT INTO `list_song` VALUES (36, 82, 11);
INSERT INTO `list_song` VALUES (37, 65, 11);
INSERT INTO `list_song` VALUES (38, 50, 11);
INSERT INTO `list_song` VALUES (39, 67, 14);
INSERT INTO `list_song` VALUES (40, 78, 14);
INSERT INTO `list_song` VALUES (41, 26, 14);
INSERT INTO `list_song` VALUES (42, 4, 15);
INSERT INTO `list_song` VALUES (43, 7, 15);
INSERT INTO `list_song` VALUES (44, 21, 15);
INSERT INTO `list_song` VALUES (45, 24, 16);
INSERT INTO `list_song` VALUES (46, 40, 16);
INSERT INTO `list_song` VALUES (47, 50, 16);
INSERT INTO `list_song` VALUES (48, 70, 16);
INSERT INTO `list_song` VALUES (49, 72, 17);
INSERT INTO `list_song` VALUES (50, 73, 17);
INSERT INTO `list_song` VALUES (51, 51, 18);
INSERT INTO `list_song` VALUES (52, 52, 18);
INSERT INTO `list_song` VALUES (53, 65, 18);
INSERT INTO `list_song` VALUES (54, 67, 18);
INSERT INTO `list_song` VALUES (55, 2, 19);
INSERT INTO `list_song` VALUES (56, 7, 19);
INSERT INTO `list_song` VALUES (57, 55, 19);
INSERT INTO `list_song` VALUES (58, 53, 19);
INSERT INTO `list_song` VALUES (59, 54, 19);
INSERT INTO `list_song` VALUES (60, 4, 20);
INSERT INTO `list_song` VALUES (61, 7, 20);
INSERT INTO `list_song` VALUES (62, 11, 20);
INSERT INTO `list_song` VALUES (63, 26, 20);
INSERT INTO `list_song` VALUES (64, 99, 21);
INSERT INTO `list_song` VALUES (65, 100, 21);
INSERT INTO `list_song` VALUES (66, 86, 21);
INSERT INTO `list_song` VALUES (67, 91, 22);
INSERT INTO `list_song` VALUES (68, 94, 22);
INSERT INTO `list_song` VALUES (69, 77, 22);
INSERT INTO `list_song` VALUES (70, 68, 22);
INSERT INTO `list_song` VALUES (71, 50, 22);
INSERT INTO `list_song` VALUES (72, 76, 17);
INSERT INTO `list_song` VALUES (73, 93, 15);
INSERT INTO `list_song` VALUES (74, 92, 15);
INSERT INTO `list_song` VALUES (75, 78, 72);
INSERT INTO `list_song` VALUES (76, 79, 72);
INSERT INTO `list_song` VALUES (77, 80, 72);
INSERT INTO `list_song` VALUES (78, 64, 71);
INSERT INTO `list_song` VALUES (79, 65, 71);
INSERT INTO `list_song` VALUES (80, 50, 71);
INSERT INTO `list_song` VALUES (81, 51, 71);
INSERT INTO `list_song` VALUES (82, 51, 70);
INSERT INTO `list_song` VALUES (83, 50, 70);
INSERT INTO `list_song` VALUES (84, 64, 62);
INSERT INTO `list_song` VALUES (85, 65, 62);
INSERT INTO `list_song` VALUES (86, 66, 62);
INSERT INTO `list_song` VALUES (87, 67, 62);
INSERT INTO `list_song` VALUES (88, 25, 63);
INSERT INTO `list_song` VALUES (89, 26, 63);
INSERT INTO `list_song` VALUES (90, 79, 63);
INSERT INTO `list_song` VALUES (91, 65, 64);
INSERT INTO `list_song` VALUES (92, 64, 64);
INSERT INTO `list_song` VALUES (93, 80, 64);
INSERT INTO `list_song` VALUES (94, 25, 65);
INSERT INTO `list_song` VALUES (95, 64, 65);
INSERT INTO `list_song` VALUES (96, 67, 67);
INSERT INTO `list_song` VALUES (97, 64, 67);
INSERT INTO `list_song` VALUES (98, 25, 67);
INSERT INTO `list_song` VALUES (99, 25, 69);
INSERT INTO `list_song` VALUES (100, 24, 69);
INSERT INTO `list_song` VALUES (101, 25, 69);
INSERT INTO `list_song` VALUES (102, 26, 69);
INSERT INTO `list_song` VALUES (103, 48, 69);
INSERT INTO `list_song` VALUES (104, 80, 68);
INSERT INTO `list_song` VALUES (105, 64, 68);
INSERT INTO `list_song` VALUES (106, 25, 68);
INSERT INTO `list_song` VALUES (107, 67, 66);
INSERT INTO `list_song` VALUES (108, 64, 66);
INSERT INTO `list_song` VALUES (109, 80, 66);
INSERT INTO `list_song` VALUES (110, 102, 23);
INSERT INTO `list_song` VALUES (112, 101, 25);
INSERT INTO `list_song` VALUES (113, 102, 30);
INSERT INTO `list_song` VALUES (114, 102, 32);
INSERT INTO `list_song` VALUES (115, 101, 34);
INSERT INTO `list_song` VALUES (116, 42, 36);
INSERT INTO `list_song` VALUES (117, 43, 36);
INSERT INTO `list_song` VALUES (118, 41, 36);
INSERT INTO `list_song` VALUES (119, 36, 38);
INSERT INTO `list_song` VALUES (120, 37, 38);
INSERT INTO `list_song` VALUES (121, 101, 38);
INSERT INTO `list_song` VALUES (122, 101, 37);
INSERT INTO `list_song` VALUES (123, 102, 39);
INSERT INTO `list_song` VALUES (124, 37, 40);
INSERT INTO `list_song` VALUES (125, 108, 40);
INSERT INTO `list_song` VALUES (126, 102, 40);
INSERT INTO `list_song` VALUES (127, 112, 41);
INSERT INTO `list_song` VALUES (128, 102, 41);
INSERT INTO `list_song` VALUES (129, 102, 42);
INSERT INTO `list_song` VALUES (130, 41, 24);
INSERT INTO `list_song` VALUES (131, 100, 23);
INSERT INTO `list_song` VALUES (132, 98, 47);
INSERT INTO `list_song` VALUES (133, 61, 47);
INSERT INTO `list_song` VALUES (134, 62, 47);
INSERT INTO `list_song` VALUES (135, 33, 49);
INSERT INTO `list_song` VALUES (136, 68, 49);
INSERT INTO `list_song` VALUES (137, 33, 49);
INSERT INTO `list_song` VALUES (138, 23, 49);
INSERT INTO `list_song` VALUES (139, 33, 50);
INSERT INTO `list_song` VALUES (140, 21, 50);
INSERT INTO `list_song` VALUES (141, 61, 52);
INSERT INTO `list_song` VALUES (142, 62, 52);
INSERT INTO `list_song` VALUES (143, 21, 60);
INSERT INTO `list_song` VALUES (144, 22, 60);
INSERT INTO `list_song` VALUES (145, 23, 60);
INSERT INTO `list_song` VALUES (146, 63, 58);
INSERT INTO `list_song` VALUES (147, 98, 58);
INSERT INTO `list_song` VALUES (148, 63, 53);
INSERT INTO `list_song` VALUES (149, 30, 54);
INSERT INTO `list_song` VALUES (150, 61, 56);
INSERT INTO `list_song` VALUES (151, 63, 56);
INSERT INTO `list_song` VALUES (152, 98, 57);
INSERT INTO `list_song` VALUES (153, 32, 54);
INSERT INTO `list_song` VALUES (154, 22, 57);
INSERT INTO `list_song` VALUES (155, 98, 59);
INSERT INTO `list_song` VALUES (156, 63, 59);
INSERT INTO `list_song` VALUES (157, 62, 61);
INSERT INTO `list_song` VALUES (158, 22, 61);
INSERT INTO `list_song` VALUES (159, 68, 51);
INSERT INTO `list_song` VALUES (160, 35, 51);
INSERT INTO `list_song` VALUES (161, 32, 51);
INSERT INTO `list_song` VALUES (162, 33, 61);
INSERT INTO `list_song` VALUES (163, 86, 43);
INSERT INTO `list_song` VALUES (164, 100, 44);
INSERT INTO `list_song` VALUES (165, 87, 45);
INSERT INTO `list_song` VALUES (166, 86, 45);
INSERT INTO `list_song` VALUES (167, 100, 44);
INSERT INTO `list_song` VALUES (168, 88, 46);
INSERT INTO `list_song` VALUES (169, 99, 73);
INSERT INTO `list_song` VALUES (170, 88, 74);
INSERT INTO `list_song` VALUES (171, 99, 74);
INSERT INTO `list_song` VALUES (172, 88, 73);
INSERT INTO `list_song` VALUES (173, 103, 78);
INSERT INTO `list_song` VALUES (174, 103, 84);
INSERT INTO `list_song` VALUES (175, 103, 75);
INSERT INTO `list_song` VALUES (176, 103, 76);
INSERT INTO `list_song` VALUES (177, 103, 77);
INSERT INTO `list_song` VALUES (178, 103, 79);
INSERT INTO `list_song` VALUES (179, 88, 80);
INSERT INTO `list_song` VALUES (180, 99, 80);
INSERT INTO `list_song` VALUES (181, 103, 80);
INSERT INTO `list_song` VALUES (182, 104, 80);
INSERT INTO `list_song` VALUES (183, 104, 81);
INSERT INTO `list_song` VALUES (184, 88, 82);
INSERT INTO `list_song` VALUES (185, 99, 82);
INSERT INTO `list_song` VALUES (186, 105, 83);
INSERT INTO `list_song` VALUES (187, 99, 48);
INSERT INTO `list_song` VALUES (188, 95, 26);
INSERT INTO `list_song` VALUES (189, 96, 27);
INSERT INTO `list_song` VALUES (190, 97, 26);
INSERT INTO `list_song` VALUES (191, 95, 28);
INSERT INTO `list_song` VALUES (192, 98, 29);
INSERT INTO `list_song` VALUES (193, 62, 29);
INSERT INTO `list_song` VALUES (194, 87, 31);
INSERT INTO `list_song` VALUES (195, 61, 31);
INSERT INTO `list_song` VALUES (196, 63, 31);
INSERT INTO `list_song` VALUES (197, 87, 55);
INSERT INTO `list_song` VALUES (198, 96, 55);
INSERT INTO `list_song` VALUES (199, 98, 33);
INSERT INTO `list_song` VALUES (200, 63, 33);
INSERT INTO `list_song` VALUES (201, 105, 83);
INSERT INTO `list_song` VALUES (202, 106, 83);
INSERT INTO `list_song` VALUES (203, 107, 53);
INSERT INTO `list_song` VALUES (204, 107, 60);
INSERT INTO `list_song` VALUES (205, 108, 8);
INSERT INTO `list_song` VALUES (206, 112, 24);
INSERT INTO `list_song` VALUES (207, 113, 40);
INSERT INTO `list_song` VALUES (208, 109, 8);
INSERT INTO `list_song` VALUES (209, 107, 23);
INSERT INTO `list_song` VALUES (210, 1, 1);
INSERT INTO `list_song` VALUES (211, 2, 1);
INSERT INTO `list_song` VALUES (212, 3, 1);
INSERT INTO `list_song` VALUES (213, 139, 4);
INSERT INTO `list_song` VALUES (214, 138, 4);
INSERT INTO `list_song` VALUES (215, 137, 4);
INSERT INTO `list_song` VALUES (216, 136, 4);

-- ----------------------------
-- Table structure for rank
-- ----------------------------
DROP TABLE IF EXISTS `rank`;
CREATE TABLE `rank`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `songListId` bigint(20) UNSIGNED NOT NULL,
  `consumerId` bigint(20) UNSIGNED NOT NULL,
  `score` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `consumerId`(`consumerId`, `songListId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rank
-- ----------------------------
INSERT INTO `rank` VALUES (1, 2, 1, 7);
INSERT INTO `rank` VALUES (2, 2, 2, 3);
INSERT INTO `rank` VALUES (3, 1, 1, 4);
INSERT INTO `rank` VALUES (7, 13, 1, 5);
INSERT INTO `rank` VALUES (19, 21, 1, 5);
INSERT INTO `rank` VALUES (20, 31, 1, 5);
INSERT INTO `rank` VALUES (21, 5, 1, 0);
INSERT INTO `rank` VALUES (24, 11, 1, 4);
INSERT INTO `rank` VALUES (25, 10, 1, 10);
INSERT INTO `rank` VALUES (27, 6, 1, 6);
INSERT INTO `rank` VALUES (28, 7, 1, 10);
INSERT INTO `rank` VALUES (29, 1, 26, 4);
INSERT INTO `rank` VALUES (30, 7, 26, 2);
INSERT INTO `rank` VALUES (32, 3, 26, 5);
INSERT INTO `rank` VALUES (33, 14, 26, 9);

-- ----------------------------
-- Table structure for singer
-- ----------------------------
DROP TABLE IF EXISTS `singer`;
CREATE TABLE `singer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` tinyint(4) NULL DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` datetime NULL DEFAULT NULL,
  `location` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of singer
-- ----------------------------
INSERT INTO `singer` VALUES (1, '隔壁老樊', 1, '/img/singerPic/16493262059766.jpg', '1982-12-20 18:50:07', '中国四川', '华语歌坛新生代领军人物，偶像与实力兼具的超人气天王。2004年出道至今，已发行9张高品质唱片，唱片销量称冠内地群雄。2008年以来举办过9场爆满的个人演唱会，在各大权威音乐奖项中先后21次获得“最受欢迎男歌手”称号，2012年度中国TOP排行榜内地最佳男歌手，2010年在韩国M-net亚洲音乐大赏(MAMA)上获得“亚洲之星”（Best Asian Artist）大奖，影响力触及海外。');
INSERT INTO `singer` VALUES (44, '王靖雯', 0, '/img/singerPic/16493240377681.jpg', '2022-04-07 00:00:00', '中国', '王靖雯，艺名王靖雯不胖 [17]  ，2001年4月5日出生于黑龙江省，中国内地女歌手，代表作品有《爱，存在》、《不知所措》、《善变》、《沦陷》等。 [11]  [31]  ，酷狗星曜计划战略音乐人 [11]  。\r\n2020年4月，开始在短视频平台发布翻唱视频，以自弹自唱的方式出圈 [18]  ；6月，发布并推出歌曲《爱，存在》全网总播放量已突破4亿 [1-2]  ；7月，发行推出歌曲《不知所措》 [4]  ；9月，发行推出歌曲《善变》《永不失联的爱》 [3]  [9]  ；11月，发布推出歌曲《遗憾也');
INSERT INTO `singer` VALUES (45, '林俊杰', 1, '/img/singerPic/16493845496826.jpg', '2022-04-08 09:58:16', '中国福建省厦门市同安区', '林俊杰（JJ Lin），1981年3月27日出生于新加坡，祖籍中国福建省厦门市同安区，华语流行乐男歌手、作曲人、音乐制作人、潮牌主理人。\r\n2003年发行首张创作专辑《乐行者》。2004年凭借专辑《第二天堂》中的主打歌《江南》获得广泛关注。2014年凭借专辑《因你而在》夺得第25届台湾金曲奖最佳国语男歌手奖。2016年凭借专辑《和自己对话》获得第27届台湾金曲奖最佳国语男歌手奖，并推出个人首部音乐纪录片《听·见林俊杰》。截止到2019年，已发行13张正式专辑，累计创作数百首歌曲。\r\n2007年成立个人音乐');
INSERT INTO `singer` VALUES (46, '虎二', 1, '/img/singerPic/1649815490272虎.jpg', '1986-07-10 00:00:00', '中国', '虎二，Tiger Wang。80后热爱音乐和唱歌的大叔，本职IT技术“猿”。');
INSERT INTO `singer` VALUES (47, '陈奕迅', 1, '/img/singerPic/16500740474934d086e061d950a7b0208d28d559b75d9f2d3572c636d.jpg', '1974-07-27 00:00:00', '中国香港', '陈奕迅（Eason Chan），1974年7月27日出生于中国香港，祖籍广东省东莞市 [1]  ，华语流行乐男歌手、演员、作曲人，毕业于英国金斯顿大学。');
INSERT INTO `singer` VALUES (48, '莫叫姐姐', 0, '/img/singerPic/1650074830850e1fe9925bc315c6034a8cc7b7de1dc1349540923d95c.jpg', '1995-02-12 00:00:00', '中国', '驻唱歌手一枚； 吉他弹唱歌手。爱好民谣、爵士。 我很平凡，但我独一无二； 感谢喜欢，不负相遇。');

-- ----------------------------
-- Table structure for song
-- ----------------------------
DROP TABLE IF EXISTS `song`;
CREATE TABLE `song`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `singer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NOT NULL COMMENT '发行时间',
  `update_time` datetime NOT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lyric` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of song
-- ----------------------------
INSERT INTO `song` VALUES (1, 1, '隔壁老樊 - 得不到你', '明天过后', '2018-12-26 08:40:14', '2019-04-25 20:14:20', '/img/songPic/haikuotiankong.jpg', '[ti:仰望星空]\n[ar:张杰]\n[00:00.266]\n[00:00.570]作曲：曾明维（Taiwan）\n[00:00.670]作词 : 邹振东\n[00:00.770]这一天我开始仰望星空发现\n[00:07.238]星并不远梦并不远只要你踮起脚尖\n[00:17.11]我相信有一双手把我轻轻牵到你的跟前\n[00:29.233]我相信有一根线将梦想与现实相连\n[00:45.389]我相信有一种缘会把所有的偶然都实现\n[00:57.346]我相信就是这一天命运开始改变\n[01:08.85]这一天我开始仰望星空发现\n[01:14.559]星并不远梦并不远只要你踮起脚尖\n[01:22.154]我从此不再彷徨也不再腼腆\n[01:28.621]张开双臂和你一起飞的更高看的更远\n[01:41.286]Music\n[01:52.845]我相信有一种缘会把所有的偶然都实现\n[02:04.543]我相信就是这一天命运开始改变\n[02:15.135]这一天我开始仰望星空发现\n[02:21.668]星并不远梦并不远只要你踮起脚尖\n[02:29.235]我从此不再彷徨也不再腼腆\n[02:35.717]张开双臂和你一起飞的更高看的更远\n[02:48.45]开始仰望星空感觉爱的时间空间\n[02:55.353]寻找生命中最灿烂的亮点\n[03:01.278]这一天我开始仰望星空发现\n[03:07.439]星并不远梦并不远只要你踮起脚尖\n[03:14.888]我从此不再彷徨也不再腼腆\n[03:21.630]张开双臂和你一起飞的更高看的更远\n[03:29.404]这一天我开始仰望星空发现\n[03:35.634]星并不远梦并不远只要你踮起脚尖\n[03:43.477]我从此不再彷徨也不再腼腆\n[03:49.834]张开双臂和你一起飞的更高看的更远\n[04:02.854]', '/song/隔壁老樊 - 得不到你.mp3');
INSERT INTO `song` VALUES (2, 1, '隔壁老樊 - 多想在平庸的生活拥抱你', '爱，不解释', '2018-12-26 11:24:13', '2019-05-24 00:15:25', '/img/songPic/tabudong1.jpg', '[00:00.00] 作曲 : 周振霆/代岳东\n[00:01.00] 作词 : 唐湘智\n[00:19.410]他留给你是背影\n[00:25.060]关于爱情只字不提\n[00:29.850]害你哭红了眼睛\n[00:34.780]他把谎言说的竟然那么动听\n[00:40.750]他不止一次骗了你\n[00:44.710]不值得你再为他伤心\n[00:48.380]他不懂你的心假装冷静\n[00:53.180]他不懂爱情把它当游戏\n[00:56.170]他不懂表明相爱这件事\n[01:00.060]除了对不起就只剩叹息\n[01:04.060]他不懂你的心为何哭泣\n[01:08.950]窒息到快要不能呼吸\n[01:16.700]喔喔\n[01:18.880]他不懂你的心\n[01:23.230]\n[01:36.550]他把回忆留给你\n[01:43.640]连同忧伤强加给你\n[01:48.300]对你说来不公平\n[01:54.610]他的谎言句句说的那么动听\n[01:58.750]他不止一次骗了你\n[02:03.640]不值得你再为他伤心\n[02:07.980]他不懂你的心假装冷静\n[02:11.880]他不懂爱情把它当游戏\n[02:15.720]他不懂表明相爱这件事\n[02:19.630]除了对不起就只剩叹息\n[02:23.960]他不懂你的心为何哭泣\n[02:27.970]窒息到快要不能呼吸\n[02:36.070]喔喔\n[02:38.630]他不懂你的心\n[02:42.630]\n[02:56.250]他不懂你的心假装冷静\n[02:59.990]他不懂爱情把它当游戏\n[03:03.860]他不懂表明相爱这件事\n[03:07.720]除了对不起就只剩叹息\n[03:11.570]他不懂你的心为何哭泣\n[03:15.640]窒息到快要不能呼吸\n[03:23.800]喔喔\n[03:26.320]他不懂你的心\n[03:49.834]张开双臂和你一起飞的更高看的更远\n[04:02.854]', '/song/隔壁老樊 - 多想在平庸的生活拥抱你.mp3');
INSERT INTO `song` VALUES (3, 1, '隔壁老樊 - 关于孤独我想说的话', '明天过后', '2018-12-26 11:34:31', '2018-12-26 11:34:31', '/img/songPic/haikuotiankong.jpg', '[00:00.00] 作曲 : 刘吉宁\n[00:01.00] 作词 : 周毅\n[00:26.800]烽烟起寻爱似浪淘沙\n[00:33.700]遇见她如春水映梨花\n[00:40.400]挥剑断天涯相思轻放下\n[00:46.900]梦中我痴痴牵挂\n[00:53.900]顾不顾将相王侯\n[00:55.800]管不管万世千秋\n[00:57.800]求只求爱化解\n[00:58.900]这万丈红尘纷乱永无休\n[01:01.400]爱更爱天长地久\n[01:02.800]要更要似水温柔\n[01:04.800]谁在乎谁主春秋\n[01:06.900]一生有爱何惧风飞沙\n[01:10.100]悲白发留不住芳华\n[01:13.900]抛去江山如画换她笑面如花\n[01:17.100]抵过这一生空牵挂\n[01:20.100]心若无怨爱恨也随他\n[01:23.300]天地大情路永无涯\n[01:27.100]只为她袖手天下\n[01:32.900]\n[02:00.900]顾不顾将相王侯\n[02:02.700]管不管万世千秋\n[02:03.900]求只求爱化解\n[02:05.800]这万丈红尘纷乱永无休\n[02:07.900]爱更爱天长地久\n[02:09.900]要更要似水温柔\n[02:11.800]谁在乎谁主春秋\n[02:13.400]一生有爱何惧风飞沙\n[02:16.700]悲白发留不住芳华\n[02:20.900]抛去江山如画换她笑面如花\n[02:24.600]抵过这一生空牵挂\n[02:26.900]心若无怨爱恨也随他\n[02:30.700]天地大情路永无涯\n[02:33.900]只为她袖手天下\n[02:39.900]\n[02:40.300]一生有爱何惧风飞沙\n[02:43.700]悲白发留不住芳华\n[02:47.400]抛去江山如画换她笑面如花\n[02:50.900]抵过这一生空牵挂\n[02:53.900]心若无怨爱恨也随他\n[02:56.900]天地大情路永无涯\n[02:59.900]只为她袖手天下\n[03:05.900]\n[03:06.900]烽烟起寻爱似浪淘沙\n[03:13.300]遇见她如春水映梨花\n[03:20.100]挥剑断天涯相思轻放下\n[03:26.900]梦中我痴痴牵挂\n[03:32.900]', '/song/隔壁老樊 - 关于孤独我想说的话.mp3');
INSERT INTO `song` VALUES (4, 1, '隔壁老樊 - 你的姑娘', '再爱我一回', '2018-12-26 11:47:15', '2019-04-24 21:13:52', '/img/songPic/haikuotiankong.jpg', '[00:00.00] 作曲 : 黎沸辉\n [00:01.00] 作词 : 黎沸辉\n[00:04.16]如果爱\n[00:16.77]ah~~~ah~~\n[00:34.50]我的心从没有搬到另一个地址\n[00:40.57]还是和你用同样一室的钥匙\n[00:48.19]你的眼泪一滴一滴将回忆淋湿\n[00:54.64]你的拥抱却让呼吸变得真实\n[01:02.40]相爱的人 我能如何选择\n[01:09.29]伤痛和快乐全都是重复的规则\n[01:16.22]如果爱只是拉拉扯扯\n[01:19.65]两个人都动弹不得\n[01:23.10]如果爱已经少了快乐\n[01:26.68]为何心痛不能割舍\n[01:30.25]如果爱已经慢慢褪色\n[01:33.83]两颗心都失去颜色\n[01:37.25]如果爱已是非爱不可\n[01:40.79]又何必问他是否值得\n[01:44.54]可爱情在猜疑下渐渐冰冷\n[02:01.08]我的心从没有搬到另一个地址\n[02:12.48]还是和你用同样一室的钥匙\n[02:20.14]你的眼泪一滴一滴将回忆淋湿\n[02:26.32]你的拥抱却让呼吸变得真实\n[02:34.11]相爱的人 我能如何选择\n[02:41.14]伤痛和快乐全都是重复的规则\n[02:45.35]如果爱只是拉拉扯扯\n[02:51.65]两个人都动弹不得\n[02:54.99]如果爱已经少了快乐\n[02:58.80]为何心痛不能割舍\n[03:02.11]如果爱已经慢慢褪色\n[03:05.67]两颗心都失去颜色\n[03:09.14]如果爱已是非爱不可\n[03:12.70]又何必问他是否值得\n[03:16.50]看爱情在猜疑下～\n[03:20.04]如果爱只是拉拉扯扯\n[03:23.02]两个人都动弹不得\n[03:25.41]如果爱已经少了快乐\n[03:30.42]为何心痛不能割舍\n[03:33.76]如果爱已经慢慢褪色\n[03:37.30]两颗心都失去颜色\n[03:40.74]如果爱已是非爱不可\n[03:44.17]又何必问他是否值得\n[03:48.39]看爱情在猜疑下渐渐冰冷\n[03:52.17]又何必问他是否值得\n[03:55.76]如果爱已是非爱 非爱不可～\n[03:59.28]两颗心都失去了颜色\n[04:03.00]看爱情在猜疑下渐渐冰冷\n[04:06.58]又何必问他是否值得\n[04:09.79]如果爱已是非爱 非爱不可～\n[04:13.49]两颗心都失去了颜色', '/song/隔壁老樊 - 你的姑娘.mp3');
INSERT INTO `song` VALUES (124, 44, '王靖雯-不知所措', '王靖雯不胖', '2022-04-07 17:39:12', '2022-04-07 17:51:00', '/img/songPic/tubiao.jpg', '[00:00.000]不知所措 - 王靖雯不胖\r\n[00:03.632]词：大月@小分队\r\n[00:03.742]曲：陈潮成\r\n[00:03.876]编曲：赵建飞\r\n[00:04.032]录音：李亚男\r\n[00:04.205]吉他：周少伟\r\n[00:05.160]和声：赫拉\r\n[00:05.299]混音：Link\r\n[00:05.386]制作人：陈潮成\r\n[00:05.586]策划：陈爽\r\n[00:06.254]制作公司：Hikoon Music\r\n[00:07.587]OP：嗨库文化\r\n[00:07.746]「未经著作权人许可 不得翻唱 翻录或使用」\r\n[00:17.236]那时的感情 总是苦涩\r\n[00:24.407]包围着太多 无可奈何\r\n[00:31.907]你的喜怒哀乐\r\n[00:35.447]我没有温柔掌握\r\n[00:38.607]在 关键时刻\r\n[00:46.838]后来的我们 断了联络\r\n[00:54.325]规则的生活 没太多颜色\r\n[01:02.106]梦还重复做着\r\n[01:05.371]你笑着对我说如果\r\n[01:10.982]没有沉默\r\n[01:16.740]最灿烂的烟火总是先坠落\r\n[01:20.521]越是暖的经过反而越折磨\r\n[01:24.244]听再多浪漫的歌不能让我\r\n[01:29.046]解开这枷锁\r\n[01:31.787]当初该怎么说才能明白我\r\n[01:35.424]因为有你才变得脆弱\r\n[01:39.316]在我心底的角落\r\n[01:41.740]真的不知所措\r\n[02:04.192]后来的我们 断了联络\r\n[02:11.007]规则的生活 没太多颜色\r\n[02:18.646]梦还重复做着\r\n[02:22.155]你笑着对我说如果\r\n[02:28.121]没有沉默\r\n[02:31.864]最灿烂的烟火总是先坠落\r\n[02:35.392]越是暖的经过反而越折磨\r\n[02:39.224]听再多浪漫的歌不能让我\r\n[02:43.973]解开这枷锁\r\n[02:46.694]当初该怎么说才能明白我\r\n[02:50.360]因为有你才变得脆弱\r\n[02:54.118]在我心底的角落\r\n[02:56.766]真的不知所措\r\n[03:03.392]好不好 让记忆别吵\r\n[03:06.827]好不好 给我个拥抱\r\n[03:10.525]别让我一个人疯掉\r\n[03:16.735]最灿烂的烟火总是先坠落\r\n[03:20.381]越是暖的经过反而越折磨\r\n[03:24.152]听再多浪漫的歌不能让我\r\n[03:28.914]解开这枷锁\r\n[03:31.583]当初该怎么说才能明白我\r\n[03:35.318]因为有你才变得懦弱\r\n[03:39.056]在我心底的角落\r\n[03:41.968]真的不知所措', '/song/王靖雯-不知所措.mp3');
INSERT INTO `song` VALUES (125, 44, '王靖雯-会吗', '会吗 - 王靖雯不胖', '2022-04-07 17:52:13', '2022-04-07 17:52:33', '/img/songPic/tubiao.jpg', '[00:00.000]会吗 - 王靖雯不胖\r\n[00:01.688]词Lyricist：大月@小分队\r\n[00:03.376]曲Composer：箱子@小分队\r\n[00:05.064]编曲Arranger：刘也\r\n[00:06.119]吉他Guitarist：吴余涛\r\n[00:07.385]录音师Recording Engineer：李妙心\r\n[00:09.073]录音室Recording Studio：Hikoon Music Studio\r\n[00:10.761]混音工程师Mixing Engineer：张鸣利\r\n[00:12.871]母带处理Master：张鸣利\r\n[00:14.559]和声Background Vocalist：李沅芷\r\n[00:16.036]配唱制作人Vocal Producer：谭炜星@小分队\r\n[00:18.990]制作统筹Coordinator：李珑殿@小分队/黄伟@小分队\r\n[00:22.999]制作人Producer：谭炜星@小分队\r\n[00:25.320]OP：武汉伯乐爱乐\r\n[00:26.825]又一年蓦然回首\r\n[00:32.743]步履匆匆不曾停留\r\n[00:39.214]忙碌的城市\r\n[00:42.662]疲惫而温柔\r\n[00:52.162]又一季新的春秋\r\n[00:57.885]零点的钟声以后\r\n[01:05.648]是否 所愿还依旧\r\n[01:13.892]心愿会实现吗\r\n[01:16.737]会吗 会吧\r\n[01:20.240]付出总有回答\r\n[01:23.086]会吗 会吧\r\n[01:26.683]浮沉起落变化\r\n[01:30.834]会一直 在一起吗\r\n[01:39.286]未来是坦途吗\r\n[01:42.061]会吗 会吧\r\n[01:45.463]百度地图出发 到达 再出发\r\n[01:51.874]春天还会远吗\r\n[01:56.145]你看那 烟花开啊\r\n[02:34.243]Wow 每一天 昼与夜\r\n[02:36.605]朝与暮 哪怕荆棘铺满路\r\n[02:40.109]Wow 每一步 悲与欢\r\n[02:42.868]离与合 真心永不负\r\n[02:46.322]深山的鹿不知归处\r\n[02:49.535]万般皆苦无人远渡 皆自渡\r\n[02:58.496]心愿会实现吗\r\n[03:01.129]会吗 会吧\r\n[03:04.509]付出总有回答\r\n[03:07.249]会吗 会吧\r\n[03:10.928]浮沉起落变化\r\n[03:15.122]会一直 在一起吗\r\n[03:23.784]未来是坦途吗\r\n[03:26.237]会吗 会吧\r\n[03:29.698]百度地图出发 到达 再出发\r\n[03:36.092]春天还会远吗\r\n[03:40.718]你看那 烟花开啊', '/song/王靖雯-会吗.mp3');
INSERT INTO `song` VALUES (126, 44, '王靖雯-沦陷', '沦陷 - 王靖雯不胖', '2022-04-07 17:53:31', '2022-04-07 17:53:31', '/img/songPic/tubiao.jpg', '[00:00.000]沦陷 - 王靖雯不胖\r\n[00:03.393]词：乔与\r\n[00:03.975]曲：楚明玉\r\n[00:04.693]编曲：辰晨\r\n[00:05.464]和声：夏初安\r\n[00:06.363]吉他：王宝新\r\n[00:07.301]混音：张鸣利/李艺皓\r\n[00:08.923]录音：李妙心\r\n[00:09.819]人声处理：李艺皓\r\n[00:11.089]制作人：郑志宏\r\n[00:12.216]监制：郑志宏\r\n[00:13.096]策划：郑志宏\r\n[00:13.958]制作公司 ：Hikoon Music\r\n[00:14.844]推广：范瑚林/时慧萱\r\n[00:15.150]OP：嗨库文化\r\n[00:15.303]「未经著作权人许可，不得翻唱、翻录或使用」\r\n[00:15.936]你走之后整个世界\r\n[00:20.458]像是期待黎明的黑夜\r\n[00:23.598]我被自己封锁\r\n[00:25.769]在第五个不存在的季节\r\n[00:30.621]剪下了翅膀的蝴蝶\r\n[00:34.955]只能选择和玫瑰告别\r\n[00:38.506]我没想过后果\r\n[00:40.265]以为你就是我爱的一切\r\n[00:44.979]出现在 小说电影的桥段\r\n[00:49.085]镜头下拥抱 分开的画面\r\n[00:52.972]回忆情节 重合明显 模糊了从前\r\n[01:01.244]我的爱 滴滴点点 圆圆圈圈\r\n[01:04.748]像断了线\r\n[01:05.830]你曾经 心心念念 信誓旦旦\r\n[01:09.468]时间 改变 昨天 所以\r\n[01:13.300]爱会消失不见\r\n[01:15.688]我的心 明明暗暗 零零散散\r\n[01:19.293]缺氧搁浅\r\n[01:20.494]你带走 我的呼吸 不顾不管\r\n[01:24.244]爱沦陷在边缘 等待救援\r\n[01:42.952]你走之后整个世界\r\n[01:47.294]像是期待黎明的黑夜\r\n[01:50.314]我被自己封锁\r\n[01:52.653]在第五个不存在的季节\r\n[01:57.514]剪下了翅膀的蝴蝶\r\n[02:01.651]只能选择和玫瑰告别\r\n[02:04.977]我没想过后果\r\n[02:06.930]以为你就是我爱的一切\r\n[02:11.741]出现在 小说电影的桥段\r\n[02:15.813]镜头下拥抱 分开的画面\r\n[02:19.633]回忆情节 重合明显 模糊了从前\r\n[02:26.289]我的爱 滴滴点点 圆圆圈圈\r\n[02:29.451]像断了线\r\n[02:30.793]你曾经 心心念念 信誓旦旦\r\n[02:34.622]时间 改变 昨天 所以\r\n[02:38.191]爱会消失不见\r\n[02:41.157]我的心 明明暗暗 零零散散\r\n[02:44.213]缺氧搁浅\r\n[02:45.492]你带走 我的呼吸 不顾不管\r\n[02:49.269]爱沦陷在边缘 等待救援\r\n[02:55.847]我的爱 滴滴点点 圆圆圈圈\r\n[02:58.920]像断了线\r\n[03:00.201]你曾经 心心念念 信誓旦旦\r\n[03:03.813]时间 改变 昨天 所以\r\n[03:07.614]爱会消失不见\r\n[03:10.275]我的心 明明暗暗 零零散散\r\n[03:13.713]缺氧搁浅\r\n[03:15.002]你带走 我的呼吸 不顾不管\r\n[03:18.690]爱沦陷在边缘 等待救援', '/song/王靖雯-沦陷.mp3');
INSERT INTO `song` VALUES (127, 44, '王靖雯-善变', '善变 - 王靖雯不胖', '2022-04-07 17:57:31', '2022-04-07 17:57:31', '/img/songPic/tubiao.jpg', '[00:01.740]善变 - 王靖雯不胖\r\n[00:03.480]词：米嘉郁\r\n[00:05.220]曲：孙大雄\r\n[00:06.960]编曲：犹八音舍\r\n[00:08.700]制作人：一寸光年团队\r\n[00:10.440]吉他：老田\r\n[00:12.180]配唱：杨沛\r\n[00:13.920]录音：李妙心\r\n[00:15.660]混音：边策\r\n[00:17.400]和声：王韩一淋\r\n[00:19.140]制作公司/OP：一寸光年\r\n[00:20.930]身后公园 是第一次遇见\r\n[00:26.450]转角花店 见证牵手两年\r\n[00:31.940]这城市散落着太多纪念\r\n[00:36.740]总是在我们之间兜圈\r\n[00:43.030]如果我是 你眼里的景点\r\n[00:48.750]路过就好 何必留下想念\r\n[00:54.070]何必让故事用微笑开篇\r\n[00:58.840]结局眼泪悼念\r\n[01:04.690]只怪有你的从前 美的太过惊艳\r\n[01:10.860]才会当热情变浅 决裂明显\r\n[01:15.790]爱是谁也绕不开的抛物线\r\n[01:27.100]从前你穿越风雨都会仓促见一面\r\n[01:32.630]后来连伞的边缘你都懒得分一点\r\n[01:38.140]是我们低估了时间的善变\r\n[01:43.070]太轻易让浓烈的故事翻篇\r\n[01:49.190]从前你穿过半座城市陪我一起失眠\r\n[01:54.790]后来你好像跟你的方向盘更近一点\r\n[02:00.270]只是当泪水又滑落在照片\r\n[02:05.310]却舍不得删\r\n[02:19.200]只怪有你的从前 美的太过惊艳\r\n[02:25.350]才会当热情变浅 决裂明显\r\n[02:30.220]爱是谁也绕不开的抛物线\r\n[02:41.620]从前你穿越风雨都会仓促见一面\r\n[02:47.140]后来连伞的边缘你都懒得分一点\r\n[02:52.590]是我们低估了时间的善变\r\n[02:57.550]太轻易让浓烈的故事翻篇\r\n[03:03.630]从前你穿过半座城市陪我一起失眠\r\n[03:09.270]后来你好像跟你的方向盘更近一点\r\n[03:14.730]只是当泪水又滑落在照片\r\n[03:19.790]却舍不得删\r\n[03:25.770]从前你穿过半座城市陪我一起失眠\r\n[03:31.350]后来你好像跟你的方向盘更近一点\r\n[03:36.870]只是当泪水又滑落在照片\r\n[03:41.850]却舍不得删\r\n[03:47.830]好歹让这一幕幕从前 不只像谎言', '/song/王靖雯-善变.mp3');
INSERT INTO `song` VALUES (128, 44, '王靖雯-说说话', '说说话 - 王靖雯', '2022-04-07 17:58:15', '2022-04-07 17:58:15', '/img/songPic/tubiao.jpg', '[00:00.000]说说话 - 王靖雯\r\n[00:00.609]词 Lyrics：王靖雯\r\n[00:01.044]曲 Compose：王靖雯\r\n[00:01.479]编曲 Arranger：甘虎\r\n[00:01.914]制作人 Producer：顾雄\r\n[00:02.436]配唱 Vocal Producer：叶匡衡\r\n[00:03.045]鼓手 Drummer：罗彬\r\n[00:03.480]吉他 Guitar：老天宇\r\n[00:04.002]弦乐编写 Strings Arrangement：甘虎\r\n[00:04.698]弦乐监制 Strings Producer：李朋\r\n[00:05.394]弦乐 Strings：国际首席爱乐乐团\r\n[00:06.351]和声/编写 Backing Vocals：曾婕Joey.Z\r\n[00:07.221]人声录音工程师 Recording Engineer：李泽文\r\n[00:08.265]混音工程师 Mixing Engineer：顾雄\r\n[00:09.048]母带工程师 Mastering：顾雄\r\n[00:09.744]录音棚 Recording Studio：板眼录音棚（Wu Han）\r\n[00:10.701]监制 Executive producer：潇喆Sean\r\n[00:11.310]OP：Hikoon Music\r\n[00:11.571]【未经著作权人许可 不得翻唱翻录或使用】\r\n[00:13.141]你在哪啊\r\n[00:17.011]还是老样子吗\r\n[00:21.250]脾气还是那么差\r\n[00:28.077]上班会迟到吗\r\n[00:31.693]饭有按时吃吗\r\n[00:34.968]是不是还那么晚回家\r\n[00:40.601]你在干嘛\r\n[00:44.774]会偶尔想想我吗\r\n[00:48.234]我只是想和你说说话\r\n[00:55.119]我最近过得还好啊\r\n[00:58.557]我这里飘起了雪花\r\n[01:01.933]可我找不到你 我好想你\r\n[01:05.757]我没有办法\r\n[01:09.389]我们 也曾是对方唯一的希望\r\n[01:15.984]现在 谁又代替我出场\r\n[01:23.363]我总让你失望\r\n[01:26.901]夜还如此漫长\r\n[01:30.155]我不奢望 只想看看你模样\r\n[01:47.663]你在干嘛\r\n[01:51.736]会偶尔想想我吗\r\n[01:55.214]我只是想和你说说话\r\n[02:01.804]我最近过得还好啊\r\n[02:05.286]我这里飘起了雪花\r\n[02:08.664]可我找不到你 我好想你\r\n[02:12.577]我没有办法\r\n[02:16.187]我们 也曾是对方唯一的希望\r\n[02:22.872]现在 谁又代替我出场\r\n[02:30.233]我总让你失望\r\n[02:33.739]夜还如此漫长\r\n[02:36.999]我不奢望 只想看看你模样\r\n[02:43.864]我不再是你生命的一束光\r\n[02:50.402]你会 靠在 另一个肩膀\r\n[02:57.709]就这样散了场\r\n[03:00.970]就算夜再漫长\r\n[03:04.435]别回头望 请忘了我模样\r\n[03:11.209]就这样散了场\r\n[03:14.770]就算人来人往\r\n[03:18.262]可那几年 只有你为我鼓掌\r\n[03:27.217]你在哪啊', '/song/王靖雯-说说话.mp3');
INSERT INTO `song` VALUES (129, 44, '王靖雯-忘了没有', '忘了没有 - 王靖雯不胖', '2022-04-07 17:58:55', '2022-04-07 17:58:55', '/img/songPic/tubiao.jpg', '[00:00.000]忘了没有 - 王靖雯不胖\r\n[00:01.834]词：萧然/郑志宏\r\n[00:02.077]曲：萧然/郑志宏\r\n[00:02.297]编曲：辰晨\r\n[00:02.435]吉他：齐成刚\r\n[00:02.606]和声：夏初安\r\n[00:02.754]混音：李艺皓\r\n[00:02.932]制作团队：九宫格\r\n[00:03.154]制作人：郑志宏\r\n[00:03.355]策划：郑志宏\r\n[00:03.509]制作公司：Hikoon Music\r\n[00:03.708]OP：嗨库文化\r\n[00:03.889]「未经著作权人许可 不得翻唱 翻录或使用」\r\n[00:20.473]有没有人告诉你我不快乐\r\n[00:28.949]只剩我 独自承受\r\n[00:35.996]回想过 我牵着你的手\r\n[00:44.639]不知有多久 已没在停留\r\n[00:52.247]你到底忘了没有忘了没有忘了没有\r\n[00:56.918]我和你一起承诺每一个梦\r\n[01:01.077]每一个失眠夜晚你的晚安变成孤单\r\n[01:05.650]只能在回忆中 拥抱我\r\n[01:09.935]你到底忘了没有忘了没有忘了没有\r\n[01:14.084]你爱我是你亲口的承诺\r\n[01:18.490]我还在幻想 夜深人静的时候\r\n[01:22.722]你还是 会想我\r\n[01:27.168]有没有\r\n[01:50.534]有没有人还不经意提起我\r\n[01:58.863]错过的 我没有闪躲\r\n[02:05.904]我试过 悠悠荡荡寻着\r\n[02:14.471]找回那所有 珍惜那所有\r\n[02:22.174]你到底忘了没有忘了没有忘了没有\r\n[02:26.924]我和你一起承诺每一个梦\r\n[02:31.295]每一个失眠夜晚你的晚安变成孤单\r\n[02:35.517]只能在回忆中 拥抱我\r\n[02:39.855]你到底忘了没有忘了没有忘了没有\r\n[02:44.109]你爱我是你亲口的承诺\r\n[02:48.479]我还在幻想 夜深人静的时候\r\n[02:52.698]你还是 会想我\r\n[02:56.969]有没有\r\n[02:58.357]多少次看着你的眼睛\r\n[03:02.542]多少次想得到你回应\r\n[03:06.180]我没有勇气 却还想问你\r\n[03:10.386]你怎么会舍得放弃\r\n[03:13.776]你到底忘了没有忘了没有忘了没有\r\n[03:18.355]我和你一起承诺每一个梦\r\n[03:22.593]每一个失眠夜晚你的晚安变成孤单\r\n[03:26.856]只能在回忆中 拥抱我\r\n[03:30.717]你到底忘了没有忘了没有忘了没有\r\n[03:35.773]你爱我是你亲口的承诺\r\n[03:39.986]我还在幻想 夜深人静的时候\r\n[03:44.120]你还是 会想我\r\n[03:48.823]有没有', '/song/王靖雯-忘了没有.mp3');
INSERT INTO `song` VALUES (130, 44, '王靖雯-遗憾也值得', '遗憾也值得 - 王靖雯不胖', '2022-04-07 17:59:29', '2022-04-07 17:59:29', '/img/songPic/tubiao.jpg', '[00:00.147]遗憾也值得 - 王靖雯不胖\r\n[00:02.467]词：陆云飞\r\n[00:03.227]曲：曾斌斌\r\n[00:03.980]音乐制作人：张博文Bryan Z\r\n[00:06.243]编曲：李师珏\r\n[00:07.253]吉他：韩俊平\r\n[00:08.222]和音/和音编写：夏初安\r\n[00:09.900]混音/母带工程：李晨曦\r\n[00:11.603]统筹：东煦\r\n[00:12.347]监制：潇喆Sean\r\n[00:12.980]OP/发行：鲸鱼向海 (北京)文化有限公司\r\n[00:13.474]（未经许可 不得翻唱翻录或使用）\r\n[00:17.764]结局不完美 情节更难舍\r\n[00:24.009]牵手走过 四季颜色 记忆在重播\r\n[00:31.101]茫茫人生 多么庆幸 为你疯狂过\r\n[00:37.245]半夜去看星光 想想都笑了\r\n[00:45.222]安静的关注 你的新生活\r\n[00:51.470]旅行健身 看书电影\r\n[00:55.227]化妆进步了\r\n[00:58.619]只是可惜 在你身边\r\n[01:02.043]少了一个我\r\n[01:04.619]也许有我陪着 笑容会更多\r\n[01:10.891]希望你也曾遗憾过\r\n[01:14.475]最后却仍觉得值得\r\n[01:18.003]如果爱 难免错过 请你相信\r\n[01:22.419]也不是谁的错\r\n[01:24.980]感谢你也曾爱过我\r\n[01:28.284]给我最快乐的角色\r\n[01:32.603]若你感觉累了\r\n[01:34.570]一回头是我\r\n[01:57.202]安静的关注 你的新生活\r\n[02:03.570]旅行健身 看书电影\r\n[02:07.042]化妆进步了\r\n[02:10.257]只是可惜 在你身边\r\n[02:13.953]少了一个我\r\n[02:16.561]也许有我陪着 笑容会更多\r\n[02:23.018]希望你也曾遗憾过\r\n[02:26.393]最后却仍觉得值得\r\n[02:29.794]如果爱 难免错过 请你相信\r\n[02:34.387]也不是谁的错\r\n[02:36.850]感谢你也曾爱过我\r\n[02:40.109]给我最快乐的角色\r\n[02:44.249]若你感觉累了\r\n[02:46.441]一回头是我\r\n[02:51.274]不用再执着 最后的结果\r\n[02:54.585]你已给我最美的段落\r\n[02:57.949]你在心里住着 我不怕寂寞\r\n[03:04.057]希望你也曾遗憾过\r\n[03:07.529]最后却仍觉得值得\r\n[03:10.936]如果爱 难免错过 请你相信\r\n[03:15.592]也不是谁的错\r\n[03:18.176]感谢你也曾爱过我\r\n[03:21.328]给我最快乐的角色\r\n[03:25.352]若你感觉累了\r\n[03:27.585]一回头是我\r\n[03:32.481]若你感觉累了\r\n[03:34.562]一回头是我', '/song/王靖雯-遗憾也值得.mp3');
INSERT INTO `song` VALUES (131, 45, '林俊杰-美人鱼', '55', '2022-04-08 10:03:25', '2022-04-08 10:03:25', '/img/songPic/1649383434014ljj.jpg', '[00:00.00]《美人鱼》-林俊杰 - 我爱曲艺\r\n\r\n[00:05.29]词：简胜/林秋离\r\n\r\n[00:10.58]曲：林俊杰\r\n\r\n[00:15.87]编曲：梁定江/Casw Woo\r\n\r\n[00:21.16]制作人：梁定江/Casw Woo\r\n\r\n[00:26.45]我在沙滩划个圆圈\r\n\r\n[00:28.95]属于我俩安逸世界\r\n\r\n[00:31.46]不用和别人连线\r\n\r\n[00:36.55]我不管你来自深渊\r\n\r\n[00:39.00]也不在乎身上鳞片\r\n\r\n[00:41.54]爱情能超越一切\r\n\r\n[00:46.02]只要你在我身边\r\n\r\n[00:47.94]所有蜚语流言 完全视而不见\r\n\r\n[00:51.04]请不要匆匆一面\r\n\r\n[00:53.04]一转身就沉入海平线\r\n\r\n[00:58.66]传说中你为爱甘心被搁浅\r\n\r\n[01:03.70]我也可以为你\r\n\r\n[01:05.55]潜入海里面\r\n\r\n[01:08.39]怎么忍心断绝\r\n\r\n[01:10.95]忘记我不变的誓言\r\n\r\n[01:14.43]我眼泪断了线\r\n\r\n[01:18.90]现实里有了我对你的眷恋\r\n\r\n[01:23.91]我愿意化作雕像\r\n\r\n[01:26.10]等你出现\r\n\r\n[01:28.59]再见再也不见\r\n\r\n[01:31.16]心碎了飘荡在海边\r\n\r\n[01:34.63]你抬头就看见\r\n\r\n[01:59.87]我在沙滩划个圆圈\r\n\r\n[02:02.39]属于我俩安逸世界\r\n\r\n[02:05.00]不用和别人连线\r\n\r\n[02:09.95]我不管你来自深渊\r\n\r\n[02:12.48]也不在乎身上鳞片\r\n\r\n[02:15.02]爱情能超越一切\r\n\r\n[02:19.46]只要你在我身边\r\n\r\n[02:21.44]所有蜚语流言 完全视而不见\r\n\r\n[02:24.46]请不要匆匆一面\r\n\r\n[02:26.45]一转身就沉入海平线\r\n\r\n[02:32.08]传说中你为爱甘心被搁浅\r\n\r\n[02:37.10]我也可以为你\r\n\r\n[02:38.99]潜入海里面\r\n\r\n[02:41.84]怎么忍心断绝\r\n\r\n[02:44.39]忘记我不变的誓言\r\n\r\n[02:47.87]我眼泪断了线\r\n\r\n[02:52.32]现实里有了我对你的眷恋\r\n\r\n[02:57.33]我愿意化作雕像\r\n\r\n[02:59.57]等你出现\r\n\r\n[03:02.05]再见再也不见\r\n\r\n[03:04.59]心碎了飘荡在海边\r\n\r\n[03:08.09]你抬头就看见\r\n\r\n[03:12.55]传说中你为爱甘心被搁浅\r\n\r\n[03:17.55]我也可以为你\r\n\r\n[03:19.47]潜入海里面\r\n\r\n[03:22.27]怎么忍心断绝\r\n\r\n[03:24.78]忘记我不变的誓言\r\n\r\n[03:28.34]我眼泪断了线\r\n\r\n[03:32.73]现实里有了我对你的眷恋\r\n\r\n[03:37.75]我愿意化作雕像\r\n\r\n[03:39.96]等你出现\r\n\r\n[03:42.46]再见再也不见\r\n\r\n[03:45.00]心碎了飘荡在海边\r\n\r\n[03:48.47]你抬头就看见\r\n\r\n[03:52.63]你 你抬头就看见\r\n\r\n[03:57.64]你 你抬头就看见\r\n\r\n[04:14.00]', '/song/我爱曲艺-《美人鱼》-林俊杰.mp3');
INSERT INTO `song` VALUES (132, 45, '林俊杰-修炼爱情', '55', '2022-04-08 10:05:00', '2022-04-08 10:05:00', '/img/songPic/1649383661178l.jpg', '[00:00.28]修炼爱情 - 林俊杰\r\n[00:16.85]凭什么要失望 藏眼泪到心脏\r\n[00:28.75]往事不会说谎别跟它为难\r\n[00:35.83]我们两人之间不需要这样\r\n[00:43.16]我想\r\n[00:46.05]修炼爱情的心酸\r\n[00:49.57]学会放好以前的渴望\r\n[00:53.59]我们那些信仰 要忘记多难\r\n[01:00.24]远距离的欣赏 近距离的迷惘\r\n[01:03.88]谁说太阳会找到月亮\r\n[01:07.90]别人有的爱 我们不可能模仿\r\n[01:14.64]修炼爱情的悲欢\r\n[01:18.06]我们这些努力不简单\r\n[01:22.03]快乐炼成泪水 是一种勇敢\r\n[01:28.76]几年前的幻想 几年后的原谅\r\n[01:32.38]为一张脸去养一身伤\r\n[01:36.24]别讲想念我 我会受不了这样\r\n[01:45.50]记忆它真嚣张 路灯把痛点亮\r\n[01:57.72]情人一起看过多少次月亮\r\n[02:04.76]它在天空看过多少次遗忘\r\n[02:11.49]多少心慌\r\n[02:14.96]修炼爱情的心酸\r\n[02:18.49]学会放好以前的渴望\r\n[02:22.46]我们那些信仰 要忘记多难\r\n[02:29.10]远距离的欣赏 近距离的迷惘\r\n[02:32.78]谁说太阳会找到月亮\r\n[02:36.85]别人有的爱 我们不可能模仿\r\n[02:43.42]修炼爱情的悲欢\r\n[02:46.89]我们这些努力不简单\r\n[02:51.00]快乐炼成泪水 是一种勇敢\r\n[02:57.62]几年前的幻想 几年后的原谅\r\n[03:01.17]为一张脸去养一身伤\r\n[03:05.28]别讲想念我 我会受不了这样\r\n[03:23.40]笑着说爱让人疯狂\r\n[03:26.96]哭着说爱让人坚强\r\n[03:30.45]忘不了那个人就投降\r\n[03:38.52]修炼爱情的悲欢\r\n[03:42.04]我们这些努力不简单\r\n[03:46.02]快乐炼成泪水 是一种勇敢\r\n[03:52.68]几年前的幻想 几年后的原谅\r\n[03:56.23]为一张脸去养一身伤\r\n[04:00.15]别讲想念我 我会受不了这样\r\n[04:06.85]几年前的幻想 几年后的原谅\r\n[04:10.53]为一张脸去养一身伤\r\n[04:14.54]别讲想念我 我会受不了这样', '/song/林俊杰-修炼爱情.mp3');
INSERT INTO `song` VALUES (133, 45, '林俊杰-可惜没如果', '55', '2022-04-08 10:05:53', '2022-04-08 10:05:53', '/img/songPic/1649383667193ljj.jpg', '[00:00.61]可惜没如果 - 林俊杰\r\n[00:03.23]词：林夕 曲：林俊杰\r\n[00:24.67]假如把犯得起的错\r\n[00:28.36]能错的都错过\r\n[00:30.45]应该还来得及去悔过\r\n[00:36.52]假如没把一切说破\r\n[00:40.47]那一场小风波\r\n[00:43.65]将一笑带过\r\n[00:47.19]在感情面前\r\n[00:49.51]讲什么自我\r\n[00:52.49]要得过且过\r\n[00:56.19]才好过\r\n[00:59.09]全都怪我\r\n[01:01.07]不该沉默时沉默\r\n[01:04.51]该勇敢时软弱\r\n[01:07.60]如果不是我\r\n[01:10.56]误会自己洒脱\r\n[01:13.69]让我们难过\r\n[01:16.93]可当初的你\r\n[01:18.55]和现在的我\r\n[01:20.07]假如重来过\r\n[01:23.09]倘若那天\r\n[01:25.07]把该说的话好好说\r\n[01:28.56]该体谅的不执着\r\n[01:31.57]如果那天我\r\n[01:34.59]不受情绪挑拨\r\n[01:37.55]你会怎么做\r\n[01:41.02]那么多如果\r\n[01:42.50]可能如果我\r\n[01:44.09]可惜没如果\r\n[01:46.63]只剩下结果\r\n[02:16.97]如果早点了解\r\n[02:19.64]那率性的你\r\n[02:22.54]或者晚一点\r\n[02:25.88]遇上成熟的我\r\n[02:28.74]不过 哦。\r\n[02:32.24]全都怪我\r\n[02:34.37]不该沉默时沉默\r\n[02:37.35]该勇敢时软弱\r\n[02:40.58]如果不是我\r\n[02:43.60]误会自己洒脱\r\n[02:46.57]让我们难过\r\n[02:50.03]可当初的你\r\n[02:51.58]和现在的我\r\n[02:53.10]假如重来过\r\n[02:56.18]倘若那天\r\n[02:58.24]把该说的话好好说\r\n[03:01.55]该体谅的不执着\r\n[03:04.16]如果那天我\r\n[03:07.56]不受情绪挑拨\r\n[03:10.55]你会怎么做\r\n[03:14.07]那么多如果\r\n[03:15.54]可能如果我\r\n[03:17.04]可惜没如果\r\n[03:19.62]没有你和我\r\n[03:32.69]都怪我\r\n[03:34.55]不该沉默时沉默\r\n[03:37.48]该勇敢时软弱\r\n[03:40.57]如果不是我\r\n[03:43.49]误会自己洒脱\r\n[03:46.71]让我们难过\r\n[03:50.01]可当初的你\r\n[03:51.49]和现在的我\r\n[03:53.08]假如重来过\r\n[03:56.10]倘若那天\r\n[03:58.31]把该说的话好好说\r\n[04:01.55]该体谅的不执着\r\n[04:04.67]如果那天我\r\n[04:07.61]不受情绪挑拨\r\n[04:10.62]你会怎么做\r\n[04:14.11]那么多如果\r\n[04:15.54]可能如果我\r\n[04:17.06]可惜没如果\r\n[04:22.92]只剩下结果\r\n[04:29.03]可惜没如果', '/song/林俊杰-可惜没如果.mp3');
INSERT INTO `song` VALUES (134, 45, '林俊杰-背对背拥抱', '55', '2022-04-08 10:06:35', '2022-04-08 10:06:35', '/img/songPic/1649383671814ljj.jpg', '[00:00.88]背对背拥抱 - 林俊杰\r\n[00:14.60]话总说不清楚 该怎么明了\r\n[00:21.96]一字一句像圈套\r\n[00:29.27]旧帐总翻不完 谁无理取闹\r\n[00:36.28]你的双手甩开刚好的微妙\r\n[00:41.79]然后战火再燃烧\r\n[00:48.34]我们背对背拥抱\r\n[00:51.97]滥用沉默在咆哮\r\n[00:55.65]爱情来不及变老\r\n[00:58.63]葬送在烽火的玩笑\r\n[01:03.02]我们背对背拥抱\r\n[01:06.70]真话兜着圈子乱乱绕\r\n[01:10.73]只是想让我知道\r\n[01:14.51]只是想让你知道 爱的警告\r\n[01:22.88]话总说不清楚 该怎么明了\r\n[01:30.14]一字一句像圈套\r\n[01:37.61]旧帐总翻不完 谁无理取闹\r\n[01:44.51]你的双手甩开刚好的微妙\r\n[01:50.06]然后战火再燃烧\r\n[01:54.70]我们背对背拥抱\r\n[01:58.34]滥用沉默在咆哮\r\n[02:02.07]爱情来不及变老\r\n[02:04.99]葬送在烽火的玩笑\r\n[02:09.89]我们背对背拥抱\r\n[02:13.12]真话兜着圈子乱乱绕\r\n[02:16.95]只是想让我知道\r\n[02:20.63]只是想让你知道 爱的警告\r\n[02:29.05]我不要一直到 形同陌路变成自找\r\n[02:36.01]既然可以拥抱 就不要轻易放掉\r\n[02:44.84]我们背对背拥抱\r\n[02:48.27]滥用沉默在咆哮\r\n[02:51.90]爱情来不及变老\r\n[02:54.82]葬送在烽火的玩笑\r\n[02:59.22]我们背对背拥抱\r\n[03:03.04]真话兜着圈子乱乱绕\r\n[03:07.03]只是想让我知道\r\n[03:10.61]只是想让你知道 这警告\r\n[03:18.13]只是想让我知道\r\n[03:21.66]只是想让你知道 爱的警告', '/song/林俊杰-背对背拥抱.mp3');
INSERT INTO `song` VALUES (135, 45, '林俊杰-手心的蔷薇', '55', '2022-04-08 10:07:14', '2022-04-08 10:07:14', '/img/songPic/1649383676846l.jpg', '[00:00.38]手心的蔷薇 - 林俊杰＆G.E.M.邓紫棋\r\n[00:04.45]作词：林怡凤 作曲：林俊杰\r\n[00:06.55] Oh\r\n[00:12.23]你眼眶超载的眼泪\r\n[00:17.80]乘客是绝望和心碎\r\n[00:21.73]我可以看见你忍住伤悲\r\n[00:27.62]那一双爱笑眼睛不适合皱眉\r\n[00:33.85] 你目光独有的温暖\r\n[00:40.07]是不会熄灭的明天\r\n[00:43.89]我可以感觉\r\n[00:46.63]你没有说出口的安慰\r\n[00:50.45]远比我失去的更加珍贵\r\n[00:59.22] 手心的蔷薇\r\n[01:02.68] 刺伤而不自觉\r\n[01:05.64] 你值得被疼爱 oh\r\n[01:08.10] 你懂我的期待\r\n[01:11.04] 绚烂后枯萎 ye~yeah\r\n[01:13.89]  经过几个圆缺\r\n[01:16.64]  有我在\r\n[01:17.13]  有你在\r\n[01:21.94]  你埋藏的蔷薇\r\n[01:24.84]  你动人的香味\r\n[01:27.54]  是最好的你\r\n[01:29.21]   陪我盼我接受世界\r\n[01:34.23]不完美 另一面多美\r\n[01:48.87] 指纹写下所有遇见\r\n[01:53.03]  oh\r\n[01:54.74]  你留着\r\n[01:55.72] 心碎那一页\r\n[01:58.24]  骄傲的展现你真无所谓\r\n[02:02.94]  无所谓\r\n[02:04.18] 偶尔放纵的泪像汹涌的海水\r\n[02:10.30]  我学着一个人存在\r\n[02:14.70]  I\'m here\r\n[02:16.82]  关上灯比较不孤单\r\n[02:17.63]  不让你孤单\r\n[02:20.46]  你给的力量\r\n[02:23.35]让我在夜里无法入睡\r\n[02:24.89]  安心入睡\r\n[02:26.97]  就算没有人心疼我的泪\r\n[02:27.78]  有个人心疼你的泪\r\n[02:34.43]手心的蔷薇 oh\r\n[02:36.72] 刺伤而不自觉\r\n[02:39.51]  你值得被疼爱 my love\r\n[02:42.24]  你懂我的期待\r\n[02:45.11]  绚烂后枯萎 ye~yeah\r\n[02:47.90]  经过几个圆缺\r\n[02:50.69]  有我在 oh\r\n[02:51.03]  有你在 oh\r\n[02:55.72]  你埋藏的蔷薇\r\n[02:58.77]  你动人的香味\r\n[03:01.51]  是最好的\r\n[03:02.92]  你陪我盼我接受世界\r\n[03:08.12]不完美 另一面多美\r\n[03:18.50]  手心的蔷薇\r\n[03:21.15]  oh\r\n[03:23.56]  是带刺的纪念 ye~yeah\r\n[03:26.18]  oh\r\n[03:29.23]  整理好眼泪\r\n[03:31.67]  I am here\r\n[03:38.52]  陪我盼我接受世界\r\n[03:38.80]  陪你盼你接受世界\r\n[03:41.49]  太虚伪太善变不完美 yeah\r\n[03:47.12]天会黑心会累 ye~yeah\r\n[03:52.04]  有我在\r\n[03:52.48]  有你在\r\n[03:54.79]  什么都无畏\r\n[04:03.08]  别害怕\r\n[04:04.62] 握太紧\r\n[04:05.94]  放手\r\n[04:07.25]  蔷薇\r\n[04:08.56] 伤痕累累\r\n[04:14.35]这世界有你无畏', '/song/林俊杰&G.E.M.邓紫棋-手心的蔷薇.mp3');
INSERT INTO `song` VALUES (136, 46, '虎二 - 窗', '51', '2022-04-13 10:06:13', '2022-04-13 10:06:13', '/img/songPic/1649815812379窗.jpg', '[ver:v1.0]\r\n[ar:虎二]\r\n[ti:窗]\r\n[by:zhaoxin_karakal]\r\n[00:00.000]窗 - 虎二\r\n[00:02.049]词：虎二\r\n[00:02.522]曲：虎二\r\n[00:03.041]编曲：虎二/姚瀚霄@骁Studio\r\n[00:04.996]制作人：闫骁男@骁Studio\r\n[00:06.573]吉他：李松\r\n[00:07.256]混音/母带：百川Rebellious\r\n[00:08.178]监制：袋熊\r\n[00:08.312]企划：程程/袋熊\r\n[00:08.547]统筹：孙国润/李木子\r\n[00:08.844]发行：银河方舟\r\n[00:09.042]出品/营销：热客工作室 Hot Cake Studio\r\n[00:12.073]日坠尘芳 杯觥交错\r\n[00:17.750]新愁旧憾 与我何干\r\n[00:23.390]逢场作戏 若即若离\r\n[00:29.117]终究是客 谁诉南柯\r\n[00:37.541]轻烟飘过\r\n[00:39.123]似同窃语敲打我的窗\r\n[00:43.412]可曾是你 捎来的问安\r\n[00:49.238]粗茶一盏\r\n[00:50.616]谁在乎人间碎银几两\r\n[00:54.864]纸短情长 断了离殇\r\n[01:23.104]若是把盏尽欢酩酊能忘掉喜悲\r\n[01:28.946]又何必为俗情如癫如梦如痴如醉\r\n[01:34.584]若是看尽世间冷暖即烟消云散\r\n[01:40.095]又何需为爱恨一意孤行挂肚牵肠\r\n[01:46.259]轻烟飘过\r\n[01:47.918]似同窃语敲打我的窗\r\n[01:51.981]可曾是你 捎来的问安\r\n[01:57.552]粗茶一盏\r\n[01:59.119]谁在乎人间碎银几两\r\n[02:03.439]纸短情长 断了离殇\r\n[02:08.560]Wu\r\n[02:19.976]Wu\r\n[02:32.192]轻烟飘过\r\n[02:33.571]似同窃语敲打我的窗\r\n[02:37.891]可曾是你 捎来的问安\r\n[02:43.525]粗茶一盏\r\n[02:45.038]谁在乎人间碎银几两\r\n[02:49.261]纸短情长 断了离殇', '/song/虎二-窗.mp3');
INSERT INTO `song` VALUES (137, 46, '虎二 - 离开我你快乐吗', '51', '2022-04-13 10:07:23', '2022-04-13 10:07:23', '/img/songPic/1649815918198你应该很快乐1.jpg', '[00:00.50]离开我你快乐吗 - 虎二\r\n\r\n[00:01.18]作词：林沛涌\r\n\r\n[00:01.35]作曲：林沛涌&虎二\r\n\r\n[00:01.59]编曲：LR Music\r\n\r\n[00:01.78]和声：LR Music&虎二\r\n\r\n[00:02.04]混音：虎二\r\n\r\n[00:02.28]监制：Leo\r\n\r\n[00:02.57]OP：塑星文化\r\n\r\n[00:02.94]出品人：魏枫\r\n\r\n[00:25.78]城市又蒙上夜色\r\n\r\n[00:28.90]回家的人有几个\r\n\r\n[00:31.85]我沿着长长的街\r\n\r\n[00:34.62]在爱的身边走过\r\n\r\n[00:37.74]一片片灯火闪烁\r\n\r\n[00:40.57]淹没了月光皎洁\r\n\r\n[00:43.93]幸福是什么样的\r\n\r\n[00:46.68]被谁轻易的忽略\r\n\r\n[00:49.85]离开我你有没有\r\n\r\n[00:52.81]得到想要的快乐\r\n\r\n[00:55.67]想起你走的坚决\r\n\r\n[00:58.54]而我却总舍不得\r\n\r\n[01:01.58]离开我你有没有\r\n\r\n[01:04.71]得到想要的快乐\r\n\r\n[01:07.62]希望他和我一样\r\n\r\n[01:10.55]愿为你付出一切\r\n\r\n[01:14.63]喔......喔......喔......\r\n\r\n[01:25.75]城市又蒙上夜色\r\n\r\n[01:28.58]回家的人有几个\r\n\r\n[01:31.66]我沿着长长的街\r\n\r\n[01:34.87]在爱的身边走过\r\n\r\n[01:37.65]一片片灯火闪烁\r\n\r\n[01:40.58]淹没了月光皎洁\r\n\r\n[01:43.65]幸福是什么样的\r\n\r\n[01:46.85]被谁轻易的忽略\r\n\r\n[01:49.76]离开我你有没有\r\n\r\n[01:52.64]得到想要的快乐\r\n\r\n[01:55.68]想起你走的坚决\r\n\r\n[01:58.85]而我却总舍不得\r\n\r\n[02:01.55]离开我你有没有\r\n\r\n[02:04.67]得到想要的快乐\r\n\r\n[02:07.76]希望他和我一样\r\n\r\n[02:10.93]愿为你付出一切\r\n\r\n[02:13.94]离开我你有没有\r\n\r\n[02:16.69]得到想要的快乐\r\n\r\n[02:19.54]想起你走的坚决\r\n\r\n[02:22.58]而我却总舍不得\r\n\r\n[02:25.41]离开我你有没有\r\n\r\n[02:28.66]得到想要的快乐\r\n\r\n[02:31.52]希望他和我一样\r\n\r\n[02:34.37]愿为你付出一切\r\n\r\n[02:37.76]喔......喔......喔......\r\n\r\n[02:50.49]喔......喔......喔......\r\n\r\n[03:02.59]', '/song/虎二-离开我你快乐吗.mp3');
INSERT INTO `song` VALUES (138, 46, '虎二 - 你一定要幸福', '51', '2022-04-13 10:08:13', '2022-04-13 10:08:13', '/img/songPic/1649815968753你一定要幸福.jpg', '[ver:v1.0]\r\n[ar:虎二]\r\n[ti:你一定要幸福]\r\n[by:]\r\n[00:00.000]你一定要幸福 - 虎二\r\n[00:02.570]词：唐恬\r\n[00:05.150]曲：吴梦奇\r\n[00:07.720]编曲：吴欢\r\n[00:10.300]混音：虎二\r\n[00:12.870]发行：红人唱唱BANG\r\n[00:15.451]沿着路灯一个人走回家\r\n[00:22.280]和老朋友打电话\r\n[00:29.253]你那里天气好吗\r\n[00:34.763]有什么新闻可以当作笑话\r\n[00:42.928]回忆与我都不爱说话\r\n[00:49.728]偶尔我会想起他\r\n[00:56.458]心里有一些牵挂\r\n[01:01.715]有些爱却不得不各安天涯\r\n[01:11.527]在夜深人静的时候想起他\r\n[01:17.868]送的那些花\r\n[01:20.489]还说过一些撕心裂肺的情话\r\n[01:27.821]赌一把幸福的筹码\r\n[01:32.051]在人来人往的街头想起他\r\n[01:38.498]他现在好吗\r\n[01:41.102]可我没有能给你想要的回答\r\n[01:48.398]可是你一定要幸福呀\r\n[02:05.186]回忆与我都不爱说话\r\n[02:12.035]偶尔我会想起他\r\n[02:18.842]心里有一些牵挂\r\n[02:24.045]有些爱却不得不各安天涯\r\n[02:30.412]在夜深人静的时候想起他\r\n[02:36.781]送的那些花\r\n[02:39.349]还说过一些撕心裂肺的情话\r\n[02:46.646]赌一把幸福的筹码\r\n[02:51.066]在人来人往的街头想起他\r\n[02:57.334]他现在好吗\r\n[02:59.845]可我没有能给你想要的回答\r\n[03:07.185]可是你一定要幸福呀\r\n[03:14.965]在夜深人静的时候想起他\r\n[03:21.330]送的那些花\r\n[03:23.882]还说过一些撕心裂肺的情话\r\n[03:31.231]赌一把幸福的筹码\r\n[03:35.468]在人来人往的街头想起他\r\n[03:41.906]他现在好吗\r\n[03:44.789]可我没有能给你想要的回答\r\n[03:51.791]可是你一定要幸福呀\r\n[04:07.275]幸福呀', '/song/虎二-你一定要幸福.mp3');
INSERT INTO `song` VALUES (139, 46, '虎二 - 你应该很快乐', '51', '2022-04-13 10:08:44', '2022-04-13 10:08:44', '/img/songPic/1649815984885你应该很快乐1.jpg', '[ver:v1.0]\r\n[ar:]\r\n[ti:]\r\n[by:]\r\n[00:00.000]你应该很快乐 - 虎二 (Tiger Wang)\r\n[00:01.980]词：虎二\r\n[00:03.960]曲：虎二\r\n[00:05.950]编曲：虎二\r\n[00:07.930]和声：虎二\r\n[00:09.920]录音：虎二\r\n[00:11.900]混音：虎二\r\n[00:13.880]制作：虎二\r\n[00:15.875]你应该很快乐\r\n[00:19.869]再也没有我的打扰了\r\n[00:23.741]我有时也会觉得\r\n[00:26.608]是不是挡住了你人生的景色\r\n[00:31.471]都说孤单够了 成熟了\r\n[00:35.248]就知道这一页该翻过去了\r\n[00:39.219]在你眼里我不算什么\r\n[00:42.065]一个人的世界不也挺好的\r\n[00:47.005]我们终究也成路人了\r\n[00:50.733]就这样谁也不理谁了\r\n[00:54.756]我却挥洒最好的青春\r\n[00:58.637]作了扑火的飞蛾\r\n[01:02.541]我都没有关心的资格\r\n[01:06.179]还有什么可以欠你的\r\n[01:10.293]我都骗了自己多久了\r\n[01:14.041]以为死心塌地 早把你\r\n[01:21.896]你应该很快乐\r\n[01:25.679]再也没有我的打扰了\r\n[01:29.555]我有时也会觉得\r\n[01:32.391]是不是挡住了你人生的景色\r\n[01:37.375]都说孤单够了 成熟了\r\n[01:41.019]就知道这一页该翻过去了\r\n[01:45.080]在你眼里我不算什么\r\n[01:47.864]一个人的世界不也挺好的\r\n[01:52.709]我们终究也成路人了\r\n[01:56.495]就这样谁也不理谁了\r\n[02:00.511]我却挥洒最好的青春\r\n[02:04.452]作了扑火的飞蛾\r\n[02:08.279]我都没有关心的资格\r\n[02:12.022]还有什么可以欠你的\r\n[02:15.927]我都骗了自己多久了\r\n[02:19.846]以为死心塌地 早把你\r\n[02:24.113]看清了 放下了 松手了\r\n[02:31.838]亲爱的 对不起 打扰了\r\n[02:43.166]我们终究也成路人了\r\n[02:46.852]就这样谁也不理谁了\r\n[02:50.791]我却挥洒最好的青春\r\n[02:54.714]作了扑火的飞蛾\r\n[02:58.593]我都没有关心的资格\r\n[03:02.341]还有什么可以欠你的\r\n[03:06.336]我都骗了自己多久了\r\n[03:10.205]以为死心塌地 早把你\r\n[03:16.538]把你忘了', '/song/虎二-你应该很快乐.mp3');
INSERT INTO `song` VALUES (140, 47, '陈奕迅-孤勇者', '137', '2022-04-16 09:55:31', '2022-04-16 09:55:31', '/img/songPic/16500741399144d086e061d950a7b0208d28d559b75d9f2d3572c636d.jpg', '[ver:v1.0]\r\n[ar:陈奕迅]\r\n[ti:孤勇者]\r\n[by:p_pttzhang]\r\n[00:00.000]孤勇者 (《英雄联盟：双城之战》动画剧集中文主题曲) - 陈奕迅\r\n[00:07.433]词：唐恬\r\n[00:08.222]曲：钱雷\r\n[00:08.840]编曲：钱雷\r\n[00:09.557]吉他：高飞\r\n[00:10.357]人声录音师：利伟明\r\n[00:11.575]人声录音棚：雅旺录音室\r\n[00:12.839]Vocal edite：汝文博@SBMS Beijing\r\n[00:13.072]混音/母带：周天澈@Studio 21A\r\n[00:14.537]词版权管理方：北京梦织音传媒有限公司\r\n[00:15.406]曲版权管理方：索尼音乐版权代理（北京）有限公司\r\n[00:16.674]录音作品及MV版权：EAS MUSIC LTD\r\n[00:17.028]出品监制：霍锦/卢泓宇\r\n[00:18.329]联合出品方：拳头游戏/腾讯游戏/腾讯视频\r\n[00:19.432]制作人：钱雷\r\n[00:22.167]都是勇敢的\r\n[00:28.414]你额头的伤口你的不同你犯的错\r\n[00:36.960]都不必隐藏\r\n[00:43.412]你破旧的玩偶你的面具你的自我\r\n[00:51.571]他们说要带着光驯服每一头怪兽\r\n[00:58.744]他们说要缝好你的伤没有人爱小丑\r\n[01:05.765]为何孤独不可光荣\r\n[01:09.112]人只有不完美值得歌颂\r\n[01:13.353]谁说污泥满身的不算英雄\r\n[01:20.842]爱你孤身走暗巷\r\n[01:22.622]爱你不跪的模样\r\n[01:24.492]爱你对峙过绝望\r\n[01:26.338]不肯哭一场\r\n[01:28.235]爱你破烂的衣裳\r\n[01:29.992]却敢堵命运的枪\r\n[01:31.869]爱你和我那么像\r\n[01:33.766]缺口都一样\r\n[01:35.642]去吗 配吗 这褴褛的披风\r\n[01:39.394]战吗 战啊 以最卑微的梦\r\n[01:43.000]致那黑夜中的呜咽与怒吼\r\n[01:50.243]谁说站在光里的才算英雄\r\n[02:08.595]他们说要戒了你的狂\r\n[02:11.572]就像擦掉了污垢\r\n[02:16.152]他们说要顺台阶而上而代价是低头\r\n[02:23.452]那就让我不可乘风\r\n[02:26.527]你一样骄傲着那种孤勇\r\n[02:30.901]谁说对弈平凡的不算英雄\r\n[02:38.373]爱你孤身走暗巷\r\n[02:40.177]爱你不跪的模样\r\n[02:41.996]爱你对峙过绝望\r\n[02:43.938]不肯哭一场\r\n[02:45.750]爱你破烂的衣裳\r\n[02:47.569]却敢堵命运的枪\r\n[02:49.396]爱你和我那么像\r\n[02:51.338]缺口都一样\r\n[02:53.247]去吗 配吗 这褴褛的披风\r\n[02:56.834]战吗 战啊 以最卑微的梦\r\n[03:00.508]致那黑夜中的呜咽与怒吼\r\n[03:07.844]谁说站在光里的才算英雄\r\n[03:12.731]你的斑驳与众不同\r\n[03:19.880]你的沉默震耳欲聋\r\n[03:25.767]You Are The Hero\r\n[03:26.550]爱你孤身走暗巷\r\n[03:28.168]爱你不跪的模样\r\n[03:29.952]爱你对峙过绝望\r\n[03:31.833]不肯哭一场 (You Are The Hero)\r\n[03:33.900]爱你来自于蛮荒\r\n[03:35.481]一生不借谁的光\r\n[03:37.329]你将造你的城邦\r\n[03:39.221]在废墟之上\r\n[03:41.172]去吗 去啊 以最卑微的梦\r\n[03:44.782]战吗 战啊 以最孤高的梦\r\n[03:48.463]致那黑夜中的呜咽与怒吼\r\n[03:55.820]谁说站在光里的才算英雄', '/song/陈奕迅-孤勇者.mp3');
INSERT INTO `song` VALUES (141, 48, '莫叫姐姐-爱你爱到忘了我是谁', '47', '2022-04-16 10:08:05', '2022-04-16 10:08:05', '/img/songPic/1650074890804e1fe9925bc315c6034a8cc7b7de1dc1349540923d95c.jpg', '[ver:v1.0]\r\n[ar:莫叫姐姐]\r\n[ti:爱你爱到忘了我是谁]\r\n[by:tingting_karakal]\r\n[00:00.000]爱你爱到忘了我是谁 - 莫叫姐姐\r\n[00:02.699]词：李伦序\r\n[00:02.827]曲：李伦序\r\n[00:02.958]编曲：王子杰\r\n[00:03.129]吉他：彭炜珈\r\n[00:03.281]后期混音/母带处理：李琰祥\r\n[00:03.686]制作人：李伦序/邱老板\r\n[00:04.006]出品人：王毅Allen\r\n[00:04.202]监制：李倫序/邱昊/流水纪\r\n[00:04.612]音乐制作：古德耐听\r\n[00:04.864]OP：明白音乐娱乐\r\n[00:05.388]SP：扭湃哇音乐\r\n[00:06.064]音乐营销：北京YuGe音乐&一米观察\r\n[00:06.527]联合出品：明白音乐娱乐x众造文化x齐鼓文化\r\n[00:07.179]（未经著作权人许可，不得翻唱翻录或使用。）\r\n[00:08.592]我爱你爱到忘了我是谁\r\n[00:12.291]这一段感情我输得彻头彻尾\r\n[00:16.484]那摔碎的酒杯\r\n[00:18.573]眼泪随风飞\r\n[00:20.465]只有醉的时候我才能说出\r\n[00:23.919]心里的悲\r\n[00:32.610]我很累 真的很累\r\n[00:36.288]有些苦只能自己体会\r\n[00:40.309]当年我命里犯桃花\r\n[00:42.380]以为找到我的妃\r\n[00:44.326]谁知道最后是一场误会\r\n[00:48.499]很可悲 真的可悲\r\n[00:52.244]有些痛只能自己安慰\r\n[00:56.170]总觉得付出了全部\r\n[00:58.407]就能换来你的美\r\n[01:00.326]谁知道只换来心碎的滋味\r\n[01:04.768]我爱你爱到忘了我是谁\r\n[01:08.207]在别人眼中我只是一个傀儡\r\n[01:12.692]我对你的付出\r\n[01:14.587]你却不理会\r\n[01:16.348]以为醉的时候\r\n[01:17.844]就能忘了心里的累\r\n[01:20.604]我爱你爱到忘了我是谁\r\n[01:24.197]这一段感情我输得彻头彻尾\r\n[01:28.448]那摔碎的酒杯\r\n[01:30.547]眼泪随风飞\r\n[01:32.393]只有醉的时候我才能说出\r\n[01:37.805]心里的悲\r\n[02:14.519]很可悲真的可悲\r\n[02:18.332]有些痛只能自己安慰\r\n[02:22.067]总觉得付出了全部\r\n[02:24.397]就能换来你的美\r\n[02:26.326]谁知道只换来心碎的滋味\r\n[02:32.509]我爱你爱到忘了我是谁\r\n[02:36.134]在别人眼中我只是一个傀儡\r\n[02:40.603]我对你的付出\r\n[02:42.463]你却不理会\r\n[02:44.366]以为醉的时候\r\n[02:45.705]就能忘了心里的累\r\n[02:48.671]我爱你爱到忘了我是谁\r\n[02:52.199]这一段感情我输得彻头彻尾\r\n[02:56.555]那摔碎的酒杯\r\n[02:58.594]眼泪随风飞\r\n[03:00.374]只有醉的时候我才能说出\r\n[03:04.077]心里的悲\r\n[03:08.852]我爱你爱到忘了我是谁\r\n[03:12.205]在别人眼中我只是一个傀儡\r\n[03:16.504]我对你的付出\r\n[03:18.545]你却不理会\r\n[03:20.350]以为醉的时候\r\n[03:21.667]就能忘了心里的累\r\n[03:24.585]我爱你爱到忘了我是谁\r\n[03:28.159]这一段感情我输得彻头彻尾\r\n[03:32.573]那摔碎的酒杯\r\n[03:34.611]眼泪随风飞\r\n[03:36.309]只有醉的时候我才能说出\r\n[03:39.794]心里的悲\r\n[03:44.134]只有醉的时候我才能说出\r\n[03:48.868]心里的悲', '/song/莫叫姐姐-爱你爱到忘了我是谁.mp3');
INSERT INTO `song` VALUES (142, 48, '莫叫姐姐-不该用情', '47', '2022-04-16 10:08:55', '2022-04-16 10:08:55', '/img/songPic/1650074940869e1fe9925bc315c6034a8cc7b7de1dc1349540923d95c.jpg', '[ver:v1.0]\r\n[ar:莫叫姐姐]\r\n[ti:不该用情]\r\n[by:dongmei_karakal]\r\n[00:00.000]不该用情 (女声版) - 莫叫姐姐\r\n[00:01.528]词：乐者\r\n[00:01.873]曲：乐者\r\n[00:02.225]编曲：托尼夫斯基\r\n[00:03.106]混音：托尼夫斯基\r\n[00:16.751]无论我多么努力\r\n[00:20.742]仍无法留住你\r\n[00:25.206]点点的欢笑\r\n[00:28.279]此刻只可去追忆\r\n[00:32.470]无情地留下我\r\n[00:36.034]在风中哭泣眼睛\r\n[00:40.025]告别了昨日的爱情\r\n[00:47.131]也许当初不该用情\r\n[00:51.231]如今不会再有痴情\r\n[00:56.186]命运的注定\r\n[00:59.088]何必错对要去算清\r\n[01:02.810]也许当初她可用情\r\n[01:06.450]如今相恋没有事情\r\n[01:10.818]可惜只有我守住这份情\r\n[01:49.802]无论我多么努力\r\n[01:53.632]仍无法留住你\r\n[01:58.161]点点的欢笑\r\n[02:01.263]此刻只可去追忆\r\n[02:05.458]无情地留下我\r\n[02:08.997]在风中哭泣眼睛\r\n[02:12.724]告别了昨日的爱情\r\n[02:19.833]也许当初不该用情\r\n[02:23.903]如今不会再有痴情\r\n[02:29.145]命运的注定\r\n[02:31.834]何必错对要去算清\r\n[02:35.468]也许当初她可用情\r\n[02:39.323]如今相恋没有事情\r\n[02:43.674]可惜只有我守住这份情\r\n[02:50.960]也许当初不该用情\r\n[02:54.772]如今不会再有痴情\r\n[03:00.093]命运的注定\r\n[03:02.847]何必错对要去算清\r\n[03:06.498]也许当初她可用情\r\n[03:10.192]如今相恋没有事情\r\n[03:14.626]可惜只有我守住这份情\r\n[03:22.372]偏偏只有我守住这份情', '/song/莫叫姐姐-不该用情(女声版).mp3');
INSERT INTO `song` VALUES (143, 48, '莫叫姐姐-当我娶过她', '47', '2022-04-16 10:09:41', '2022-04-16 10:09:41', '/img/songPic/tubiao.jpg', '[ver:v1.0]\r\n[ar:莫叫姐姐]\r\n[ti:当我娶过她]\r\n[by:p_pttzhang]\r\n[00:00.000]当我娶过她 (DJ默涵版) - 莫叫姐姐\r\n[00:00.208]词：何深彰\r\n[00:00.272]曲：何深彰\r\n[00:00.336]编曲：众造文化\r\n[00:00.432]混音：李焱祥\r\n[00:00.512]监制：高天\r\n[00:00.576]制作人：众造文化/杨学涛\r\n[00:00.752]出品：众造文化\r\n[00:00.848]发行：漫吞吞文化\r\n[00:00.960]『酷狗音乐人 ?? 星曜计划』\r\n[00:01.152]全方位推广，见证星力量！\r\n[00:01.312]加入联系VX：kugou_musician\r\n[00:01.461]我睡过她的房间 喝过她的水\r\n[00:05.530]吃过她的剩饭 见过她的美\r\n[00:09.195]也见过她刚起床素颜的样子\r\n[00:12.993]这辈子就当我娶过她了吧\r\n[00:32.024]风儿轻轻吹落花\r\n[00:35.295]好想失去的她\r\n[00:39.451]年少不懂爱与恨\r\n[00:43.059]惹哭身旁那个她\r\n[00:46.940]也许这就是命运吧\r\n[00:50.285]注定我失去她\r\n[00:54.481]纷纷扰扰的世界里\r\n[00:58.200]好想曾经那个她\r\n[01:01.748]我睡过她的房间 喝过她的水\r\n[01:05.515]吃过她的剩饭 见过她的美\r\n[01:09.249]也见过她刚起床素颜的样子\r\n[01:12.991]这辈子就当我娶过她了吧\r\n[01:16.809]我承认我这辈子都忘不了她\r\n[01:20.485]甚至连做梦都想和她在一起\r\n[01:24.469]可是命运只允许我喜欢她\r\n[01:28.003]却不允许我拥有她\r\n[01:47.063]我们多久没见了\r\n[01:50.179]你还会想我吗\r\n[01:54.331]年少不懂爱与恨\r\n[01:58.129]红尘从此没有她\r\n[02:01.925]也许这就是命运吧\r\n[02:05.192]注定我失去她\r\n[02:09.407]纷纷扰扰的世界里\r\n[02:13.103]好想曾经那个她\r\n[02:16.678]我睡过她的房间 喝过她的水\r\n[02:20.576]吃过她的剩饭 见过她的美\r\n[02:24.165]也见过她刚起床素颜的样子\r\n[02:27.927]这辈子就当我娶过她了吧\r\n[02:31.673]我承认我这辈子都忘不了她\r\n[02:35.440]甚至连做梦都想和她在一起\r\n[02:39.410]可是命运只允许我喜欢她\r\n[02:42.928]却不允许我拥有她\r\n[02:46.672]我睡过她的房间 喝过她的水\r\n[02:50.491]吃过她的剩饭 见过她的美\r\n[02:54.265]也见过她刚起床素颜的样子\r\n[02:57.928]这辈子就当我娶过她了吧\r\n[03:01.659]我承认我这辈子都忘不了她\r\n[03:05.490]甚至连做梦都想和她在一起\r\n[03:09.375]可是命运只允许我喜欢她\r\n[03:12.953]却不允许我拥有她', '/song/莫叫姐姐-当我娶过她(DJ默涵版).mp3');
INSERT INTO `song` VALUES (144, 48, '莫叫姐姐-我们的爱输给了现实', '47', '2022-04-16 10:10:11', '2022-04-16 10:10:11', '/img/songPic/1650075015080e1fe9925bc315c6034a8cc7b7de1dc1349540923d95c.jpg', '[ver:v1.0]\r\n[ar:莫叫姐姐]\r\n[ti:我们的爱输给了现实]\r\n[by:tingting_karakal]\r\n[00:00.000]我们的爱输给了现实 - 莫叫姐姐\r\n[00:03.419]词：宋亚永\r\n[00:03.565]曲：王贺祺\r\n[00:03.989]编曲：Jen周周\r\n[00:05.263]制作人：刘恒锋\r\n[00:05.458]和声：Miya\r\n[00:05.559]混音：豆豆龙\r\n[00:05.730]监制：众造文化\r\n[00:06.627]OP：恒锋兴谊/金鑫大成\r\n[00:07.711]营销企划：达也/陈鱼鱼\r\n[00:08.034]总策划：包包子\r\n[00:08.228]封面设计：Zisswn\r\n[00:08.406]【未经版权方授权，不得翻唱翻录使用】\r\n[00:10.332]我们的爱输给了现实\r\n[00:13.711]也许这一切都是天意\r\n[00:17.731]曾经以为我是你的唯一\r\n[00:20.933]才发现都是骗自己\r\n[00:28.435]曾经说好要永远在一起\r\n[00:31.614]怎么走着就断了联系\r\n[00:35.904]幸福是我曾闯入的禁区\r\n[00:38.964]情一动就无药可医\r\n[00:42.845]爱情究竟是什么东西\r\n[00:45.947]为何爱着爱着就天涯分离\r\n[00:49.935]说好的要为我披上嫁衣\r\n[00:53.041]最后却把别人拥入怀里\r\n[00:58.514]我们的爱输给了现实\r\n[01:02.012]原谅我真的已经尽力\r\n[01:06.159]从此忘了那些海誓山盟\r\n[01:09.190]别在午夜偷偷哭泣\r\n[01:12.845]我们的爱输给了现实\r\n[01:16.280]也许这一切都是天意\r\n[01:20.420]曾经以为我是你的唯一\r\n[01:23.539]才发现都是骗自己\r\n[01:49.220]曾经说好要永远在一起\r\n[01:52.178]怎么走着就断了联系\r\n[01:56.256]幸福是我曾闯入的禁区\r\n[01:59.335]情一动就无药可医\r\n[02:03.391]爱情究竟是什么东西\r\n[02:06.560]为何爱着爱着就天涯分离\r\n[02:10.560]说好的要为我披上嫁衣\r\n[02:13.733]最后却把别人拥入怀里\r\n[02:19.104]我们的爱输给了现实\r\n[02:22.562]原谅我真的已经尽力\r\n[02:26.710]从此忘了那些海誓山盟\r\n[02:29.826]别在午夜偷偷哭泣\r\n[02:33.378]我们的爱输给了现实\r\n[02:36.893]也许这一切都是天意\r\n[02:40.998]曾经以为我是你的唯一\r\n[02:44.216]才发现都是骗自己\r\n[02:51.352]我们的爱输给了现实\r\n[02:54.850]原谅我真的已经尽力\r\n[02:58.876]从此忘了那些海誓山盟\r\n[03:02.090]别在午夜偷偷哭泣\r\n[03:05.778]我们的爱输给了现实\r\n[03:09.162]也许这一切都是天意\r\n[03:13.232]曾经以为我是你的唯一\r\n[03:16.402]才发现都是骗自己\r\n[03:23.976]曾经以为我是你的唯一\r\n[03:27.201]才发现都是骗自己', '/song/莫叫姐姐-我们的爱输给了现实.mp3');
INSERT INTO `song` VALUES (145, 46, '虎二-原来', '51', '2022-04-28 11:43:26', '2022-04-28 11:43:26', '/img/songPic/1651117522017u=2559212314,1218997093&fm=253&fmt=auto&app=138&f=JPEG.webp.jpg', '[ver:v1.0]\r\n[ar:]\r\n[ti:]\r\n[by:]\r\n[00:00.000]原来 - 虎二 (Tiger Wang)\r\n[00:01.950]词：虎二\r\n[00:03.910]曲：虎二\r\n[00:05.860]编曲：宋俊杰\r\n[00:07.820]木吉他：大邱\r\n[00:09.780]电吉他：王大定\r\n[00:11.730]监制：陈皓轩\r\n[00:13.690]出品：Leo\r\n[00:15.650]还有几天\r\n[00:22.178]别那么快开始想念\r\n[00:29.226]还有很多事情没做\r\n[00:36.123]怎么说着笑着就红了双眼\r\n[00:42.547]等我把行囊合上\r\n[00:48.534]为你再收拾一遍房间\r\n[00:55.326]把抱枕摆成你最喜欢的样子\r\n[01:01.686]以后不再有我的影子\r\n[01:10.660]原来平凡的故事\r\n[01:14.540]会变成孤独奢侈\r\n[01:17.932]再不能随便做你的傻子\r\n[01:24.060]傻子有你在身边\r\n[01:28.012]才是最好的画面\r\n[01:31.220]但转过身已然恍若千年\r\n[01:38.034]原来我们的起点\r\n[01:41.218]是放开彼此走远\r\n[01:44.539]而我却羡慕一生的牵眷\r\n[01:51.243]原来当说出再见\r\n[01:54.634]就再也可能不见\r\n[01:58.010]但那些我们说的承诺呢\r\n[02:03.531]都哪去了\r\n[02:17.784]等我把行囊合上\r\n[02:23.913]为你再收拾一遍房间\r\n[02:30.577]把抱枕摆成你最喜欢的样子\r\n[02:37.168]以后不再有我的影子\r\n[02:44.320]原来平凡的故事\r\n[02:47.968]会变成孤独奢侈\r\n[02:51.360]再不能随便做你的傻子\r\n[02:57.680]傻子有你在身边\r\n[03:01.400]才是最好的画面\r\n[03:04.600]但转过身已然恍若千年\r\n[03:10.984]原来我们的起点\r\n[03:14.536]是放开彼此走远\r\n[03:17.864]而我却羡慕一生的牵眷\r\n[03:24.232]原来当说出再见\r\n[03:27.816]就再也可能不见\r\n[03:31.400]但那些我们说的承诺呢\r\n[03:36.680]都哪去了\r\n[03:52.520]原来平凡的故事\r\n[03:56.232]会变成孤独奢侈\r\n[03:59.528]再不能随便做你的傻子\r\n[04:05.600]傻子有你在身边\r\n[04:09.760]才是最好的画面\r\n[04:13.112]但转过身已然恍若千年\r\n[04:19.856]原来我们的起点\r\n[04:22.920]是放开彼此走远\r\n[04:26.264]而我却羡慕一生的牵眷\r\n[04:32.744]原来当说出再见\r\n[04:36.400]就再也可能不见\r\n[04:39.647]但那些我们说的承诺呢', '/song/虎二-原来.mp3');
INSERT INTO `song` VALUES (146, 46, '虎二-笑话', '51', '2022-04-28 11:47:29', '2022-04-28 11:47:29', '/img/songPic/1651117661410u=1139518828,565297287&fm=253&fmt=auto&app=138&f=JPEG.webp.jpg', '[ver:v1.0]\r\n[ar:]\r\n[ti:]\r\n[by:]\r\n[00:00.000]风静止了 雨也停了\r\n[00:02.575]爱情 也走远了\r\n[00:05.150]作词：陈皓轩\r\n[00:07.725]作曲：陈皓轩\r\n[00:10.300]编曲：宋俊杰\r\n[00:12.875]木吉他：大邱\r\n[00:15.450]电吉他：王贵定\r\n[00:18.030]风筝飞着线却断了\r\n[00:40.320]转眼就不见了\r\n[00:47.160]关于两个人无论做什么\r\n[00:54.870]在你看来都是不对的\r\n[01:01.710]对白变的不再坦白\r\n[01:05.850]躲避显得无奈\r\n[01:09.390]还好我还有力气走开\r\n[01:15.840]我只是个笑话\r\n[01:19.410]对于你我是个傻瓜\r\n[01:23.460]看着你和他他轻抚你的头发\r\n[01:27.930]我失落的不像话\r\n[01:30.390]就当看了笑话\r\n[01:33.990]小丑睁着眼却始终不说话\r\n[01:42.210]你也有了你的他\r\n[01:45.720]我也该给你最后的祝福啦\r\n[02:03.570]关于两个人无论做什么\r\n[02:11.250]在你看来都是不对的\r\n[02:18.090]对白变的不再坦白\r\n[02:22.200]躲避显得无奈\r\n[02:25.770]还好我还有力气走开\r\n[02:32.160]我只是个笑话\r\n[02:35.790]对于你我是个傻瓜\r\n[02:39.840]看着你和他他轻抚你的头发\r\n[02:44.310]我失落的不像话\r\n[02:46.740]就当看了笑话\r\n[02:50.340]小丑睁着眼却始终不说话\r\n[02:58.470]你也有了你的他\r\n[03:02.130]我也该给你最后的祝福啦\r\n[03:30.300]我只是个笑话\r\n[03:33.990]对于你我是个傻瓜\r\n[03:37.920]相信所有的话浅尝辄止是代价\r\n[03:42.300]让我无法控制爱你的想法\r\n[03:46.740]就当看了笑话\r\n[03:50.610]小丑睁着眼却始终不说话\r\n[03:58.530]你也有了你的他\r\n[04:02.100]心痛也要告诉自己\r\n[04:05.370]笑着回答\r\n[04:10.740]当爱变了脚步停了\r\n[04:18.480]云也随风去了\r\n[04:23.850]笑话-虎二', '/song/虎二-笑话.mp3');

-- ----------------------------
-- Table structure for song_list
-- ----------------------------
DROP TABLE IF EXISTS `song_list`;
CREATE TABLE `song_list`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduction` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `style` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '无',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of song_list
-- ----------------------------
INSERT INTO `song_list` VALUES (1, '年度最佳唱作人', '/img/songListPic/16493263571016.jpg', '2019年2月3日，隔壁老樊发行歌曲《我曾》，该作品获得网易音乐第一季度音乐榜单前三名；  4月21日，《雪暴》发同名主题曲 “我曾”，  为电影《雪暴》演唱的同名主题曲正式上线；  6月8日，隔壁老樊携单曲《多想在平庸的生活拥抱你》参加北京卫视《中歌会》；   7月，首张个人专辑《我曾》正式发布；  7月25日，参加长沙芒果音乐节；  9月，单曲《失乐》荣获网易云音乐与洽洽官方联合推出“助力新声全力开嗑”最佳单曲奖； 9月12日，参加音乐真人秀《知遇之城》  ；9月14日，参加哈尔滨魔方音乐节；  9月15日，为电视剧《空降利刃》演唱片尾曲《过命的弟兄》正式上线  ；9月25日，参加云栖音乐节； 10月3日，参加黄海森林音乐节；  10月26日，参加宁波鲜氧音乐节；  11月17日，荣获《2019亚洲音乐盛典》“年度最佳唱作人”、“年度热门单曲男歌手奖”；  12月13日，空降ING中《把我曾经的故事唱给你听》的播放量195万；  12月14日，北京跨界歌王演唱《多想在平庸的生活拥抱你》； 12月，《我曾》荣登年度最热华语；  《我曾》荣登年度新上架热度最高单曲 前TOP10；  隔壁老樊被评为年度热度最高的网易音乐人前', '中国');
INSERT INTO `song_list` VALUES (2, '年轻之歌 有关爱与挑衅', '/img/songListPic/16493263784621.jpg', '那些喜欢到会循环播放的歌', '华语');
INSERT INTO `song_list` VALUES (3, '华语流行乐男歌手', '/img/songListPic/16493846896426.jpg', '并推出个人首部音乐纪录片《听·见林俊杰》。截止到2019年，已发行13张正式专辑，累计创作数百首歌曲。', '流行');
INSERT INTO `song_list` VALUES (4, '伤感歌曲', '/img/songListPic/1649816219220你应该很快乐1.jpg', '你的心声伤感歌曲', '伤感');
INSERT INTO `song_list` VALUES (5, '孤勇者', '/img/songListPic/16500743290304d086e061d950a7b0208d28d559b75d9f2d3572c636d.jpg', '孤勇者', '伤感');

SET FOREIGN_KEY_CHECKS = 1;
